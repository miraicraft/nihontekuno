<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

<title>研修・制度紹介 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイト。日本テクノの福利厚生、研修制度をご紹介します。">
<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報">
<link rel="canonical" href="#">

<!-- ページ共通のCSSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
<!-- ページ共通のCSSファイル終了-->

<!-- ページ共通のJSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
<!-- ページ共通のJSファイル終了-->

<!-- ページ固有のCSSファイル開始-->
<link rel="stylesheet" type="text/css" href="../css/training.css?v=1" media="all">
<!-- ページ固有のCSSファイル終了-->

<!-- ページ固有のJSファイル開始-->
<!-- ページ固有のJSファイル終了-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

<div class="l-pageBody">

	<nav class="l-topicPath">
		<ol itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="../">
				<span itemprop="name">RECRUIT</span></a>
				<meta itemprop="position" content="1" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				＞<a itemprop="item" href="../training/">
				<span itemprop="name">研修・制度紹介</span></a>
				<meta itemprop="position" content="2" />
			</li>
		</ol>
	</nav>

	<div class="l-content">
            <div class="p-training-top-img">
                <h2>
                    <img src="../images/training/training_out_01.png" class="sp_none" alt="研修・制度紹介" />
                    <img src="../images/training/training_out_01_sp.png" class="pc_none" alt="研修・制度紹介" />
                </h2>
       <!-- p-training-top-img --></div>
     <div class="p-training-inner">
     <h3>ワークライフバランス</h3>
        <h4>完全週休2日制</h4> <p>

             土・日・祝日（年間124日）
         </p>

       <h4>ノー残業DAY(子どもの日)</h4>
         <p>
             月末を除く毎週水曜日
         </p>

     <h4>長期有給休暇取得推進制度</h4>
         <p>

             社内制度として、平日5日連続（土日含め最大9日間）の有給休暇の取得推進
         </p>

     <h4>産休育休制度</h4>
           <p>
             社内規程に基づき男女ともに取得可能
         </p>

     <h4>時短勤務</h4>
            <p>

             子どもが小学校就学前まで取得可能 ※法令では満3歳まで
         </p>

           <h4>サークル活動</h4>
            <p>

            所属部署や役職、年齢、性別の枠を超え交流を図ることで、社員同士の親睦を深め、協調性を磨くほか、
            より豊かな感性、知識、技能などを身につけることが目的。活動支援として補助金制度も有。
         </p>

        <h4>その他</h4>
           <p>
            各種社会保険完備（雇用・労災・健康・厚生年金）、メンタルヘルスケア、退職金制度、慶弔金
         </p>

   <!-- p-training-inner --></div>

    <div class="p-training-inner">
     <h3>研修制度</h3>
             <div class="p-training-left">
                 <h4>ビジネスマナーから実践力まで、きめ細かくフォローアップ</h4>
                社会人としての基礎から、実践的な営業・技術研修まで、親切・ていねいに、
                身につくまで行います。 電気の知識や営業スキルも、研修が終わるころにはしっかりと備わり、
                自信を持ってお客様にご提案できるようになります。そして、沖縄研修では実際にテレアポを行い、
                たくさんの企業と接点を持つことによって、今後の営業の幅が広がります。
             </div>
             <div class="p-training-right">
       <img src="../images/training/training_out_02.png" alt="研修風景" />
      <a href="http://n-techno.confirm-center-s.com/content/"><img class="imghover" src="../images/training/training_out_03.png" alt="研修内容" /></a>
      </div>
    <!-- p-training-inner --></div>

	<!-- l-content --></div>


<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



