<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

<title>アンケート | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイト。新卒社員のアンケートです。">
<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報,アンケート">
<link rel="canonical" href="#">

<!-- ページ共通のCSSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
<!-- ページ共通のCSSファイル終了-->

<!-- ページ共通のJSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
<!-- ページ共通のJSファイル終了-->

<!-- ページ固有のCSSファイル開始-->
<link rel="stylesheet" type="text/css" href="../css/enquete.css?v=1" media="all">
<!-- ページ固有のCSSファイル終了-->

<!-- ページ固有のJSファイル開始-->
<!-- ページ固有のJSファイル終了-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

<div class="l-pageBody">

	<nav class="l-topicPath">
		<ol itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="../employee/">
				<span itemprop="name">MEMBER</span></a>
				<meta itemprop="position" content="1" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				＞<a itemprop="item" href="../questionnaire/">
				<span itemprop="name">社員アンケート</span></a>
				<meta itemprop="position" content="2" />
			</li>
		</ol>
	</nav>

	<div class="l-content">
<section class="p-enquete-top-img">
        <h2><img class="sp_none" src="../images/questionnaire/enquete_01.png" alt="社員の声" /><img class="pc_none" src="../images/questionnaire/enquete_01_sp.png" alt="社員の声" /></h2>
       <p> 入社3年未満の新卒入社組54名にアンケートを実施しました。<br />
		日本テクノの社員として働く若手社員の生の声をお聞きください。
        </p>
     </section>
        <div class="p-enquete-inner">
            <h3><img src="../images/questionnaire/enquete-each.png" alt="入社後に受けた印象は？"  />
            <span>入社後に受けた<br class="pc_none">印象は？</span>
            </h3>
          		<h4><img src="../images/questionnaire/enquete_3.png" alt="会社について" /><span>会社について</span></h4>
   		  <ul class="p-each-enquete">
                    <li>ひとりひとりの夢を尊重してくれて全力で応援してくれる会社です。（2014年入社/西日本営業部/女性）</li>
                    <li>元気があって、熱い会社！（2014年入社/西日本営業部/女性）</li>
                    <li>就業時間の管理がしっかりとしており、社員の事を考えてくれています。（2014年入社/東日本営業部/女性）</li>
                    <li>女も男も関係なく、年齢も関係なく、数字・結果が全て。数字を求める意識の高さを感じています。
                    （2014年入社/西日本営業部/女性）</li>
                    <li>職場環境や業務については入社前の同行で理解していたが、
                    実際にやってみると現場でしか味わえない奥深さを感じました。（2014年入社/西日本営業部/男性）</li>
                </ul>
          		<h4><img src="../images/questionnaire/enquete_4.png" alt="社員について" /><span>社員について</span></h4>
                  <ul class="p-each-enquete">
                        <li>ひとりひとりをよく見て、その人に会った教育をしてくれます。（2013年入社/西日本営業部/女性）</li>
                        <li>上長に相談できる機会が多いので、悩みもひとつひとつ解決できています。
                        （2013年入社/西日本営業部/男性）</li>
                        <li>全員が目的意識を持ち、自分の夢に向かって仕事をしています。（2014年入社/西日本営業部/女性）</li>
                        <li>生き生きと熱意を持っており、とても魅力的です。2014年入社/西日本営業部/男性）</li>
                        <li>しっかりとデータを戦略的に取り入れており、気合や根性だけでなく体と知を使っていることがわかりました。
                        （2014年入社/西日本営業部/男性）</li>
                        <li>とても丁寧に業務を教えていただいています。（2014年入社/技術サービス部/男性）</li>
          </ul>
               <h3> <img src="../images/questionnaire/enquete-each.png"  alt="部署はどんな雰囲気？" />
               <span>部署は<br class="pc_none">どんな雰囲気？</span></h3>
          <ul class="p-each-enquete">
                    <li>営業所の雰囲気はすごく明るいです。また、丁寧に教えてくれる上司が多くいます。
                    （2013年入社/西日本営業部/男性）</li>
                    <li>良いところも悪いところもはっきりと指摘してくれるので学ぶことが多くあります。
                    各自が常に成長し続けているので、日々良い影響を受けています。（2013年入社/西日本営業部/女性）
                    </li>
                    <li>チーム全体で｢前向き｣に仕事に取り組める環境です。（2013年入社/東日本営業部/男性）</li>
                    <li>支店長をはじめ、上司が気にかけてくれるため、小さなことでも相談しやすい環境です。
                    （2014年入社/東日本営業部/女性）</li>
                    <li>オンオフの切り替えがはっきりしていて、和やかな雰囲気です。（2014年入社/保安部/男性）</li>
                    <li>向上心がとても強く、勉強会などもあるため、成長できる環境が整っています。
                    （2014年入社/技術サービス部/男性）</li>
                    <li>気あいあいとした雰囲気もありますが、決して馴れ合いではなく、しっかりと「数字」に繋がっています。
                    （2014年入社/西日本営業部/女性）</li>
                  </ul>
              <h3><img src="../images/questionnaire/enquete-each.png" alt="日本テクノにどんな魅力を感じている？" />
              <span >日本テクノに<br class="pc_none">どんな魅力を感じている？</span></h3>
          <ul class="p-each-enquete">
                    <li>日本の電気事情を変えられる可能性がある。（2013年入社/保安部/男性）</li>
                    <li>新しいことに挑戦し続ける精神が根付いている。自分の意見を言える場があり、先輩方との交流も多いため、
                    いろいろな事を学べます。（2013年入社/保安部/男性）
                    </li>
                    <li>電気に関わる仕事のほとんどが日本テクノでできる。（2014年入社/技術サービス部/男性）</li>
                    <li>研修制度がしっかりとしている。｢数字｣で評価している。（2014年入社/西日本営業部/男性）</li>
                    <li>社会貢献に欠かせない商品を持っている。（2014年入社/西日本営業部/女性）</li>
                    <li>社会人としてだけでなく、人間としても成長できる環境で、実績を正当に評価してもらえる人事評価。
                    （2014年入社/西日本営業部/男性）</li>
                    <li>電力業界の発展とともに、さらなる加速を続ける可能性が高い。（2014年入社/東日本営業部/男性）</li>
                  </ul>
              <h3><img src="../images/questionnaire/enquete-each.png" alt="社会人になって生活は充実している？" /><span>社会人になって<br class="pc_none">生活は充実している？</span></h3>
          <ul class="p-each-enquete">
                    <li>日々学ぶことが多く、努力すれば目標を達成できるので仕事が楽しいです。（2014年入社/西日本営業部/女性）</li>
                    <li>仕事は忙しいですが、自分の成長を少しずつ実感できています。（2014年入社/東日本営業部/男性）</li>
                    <li>経験することの多くがはじめてなので、新鮮です。新しい経験をすることで、自分の可能性も広がっています。
                    （2014年入社/東日本営業部/女性）</li>
                    <li>土日祝日が休みのため、平日はしっかり働き、プライベートではおもいっきり遊んでいます。
                    （2014年入社/西日本営業部/男性）</li>
                    <li>オンオフの切り替えがしっかりできるため、休日も充実しています。（2014年入社/西日本営業部/女性）</li>
                    <li>評価を受けることで自分の価値を見いだせるため、公私共に充実しています。（2013年入社/電力事業部/男性）</li>
                    <li>自信を失いかけても、もう1度頑張ろうと思わせてくれる人のパワーがあります。
                    学生のときとは違う”仕事”の仲間、心から信頼できる方々にも出逢えました。（2013年入社/電力事業部/女性）</li>
                  </ul>
              <h3><img src="../images/questionnaire/enquete-each.png"　alt="就活生へのアドバイス" /><span>就活生への<br class="pc_none">アドバイス</span></h3>
          <ul class="p-each-enquete">
                    <li>自分の夢を漠然とでも持つこと、これが全てだと思います。（2013年入社/電力事業部/男性）</li>
                    <li>最低限のマナーや言葉遣いは必要だけど、そこを気にしすぎて小さくなってしまうよりは
                    堂々と自分をアピールしてほしい！（2013年入社/管理部/女性）</li>
                    <li>自分に素直に！仕事を楽しんでやれそう！そんな会社を選んでください！
                    一生ものの会社に出会えることを願っています！（2013年入社/西日本営業部/女性）</li>
                    <li>自分に合った会社を見つけるためには、できるだけ多くの会社を見てまわることが大切です。
                    （2013年入社/東日本営業部/男性）</li>
                    <li>不安も沢山あると思いますが、悔いを残さないようにしてください。私は就職活動を行うにあたって、
                    ｢人の役に立つこと｣｢インフラ企業であること｣｢社員の人柄がいいこと｣の3つを軸にして日本テクノを選びました。
                    （2014年入社/西日本営業部/女性）</li>
                    <li>自分が納得できるような良い面も悪い面も包み隠さず伝えてくれる会社を選んで、取り組むことが大切です。
                    頑張って下さい！（2014年入社/西日本営業部/男性）</li>
                    <li>自分の強みや弱みから合う合わないを判断するよりも、
                    将来的に自分の能力を引き出すことができる環境であるかどうかが重要です。（2014年入社/西日本営業部/女性）</li>
                    <li>あきらめず、妥協せずに、自分の目標に向かって頑張って下さい。（2014年入社/東日本営業部/男性）</li>
                    <li>自分らしくいられる会社を探してほしいと思います。頑張って下さい。（2014年入社/東日本営業部/女性）</li>
                    <li>｢これから自分がどうなりたいのか｣をよく考えたうえで、それを実現できる会社選びをしましょう。
                    （2014年入社/西日本営業部/男性）</li>
                  </ul>

<!-- p-enquete-inner --></div>

    <section class="p-inquiry">
			<div class="l-wrap-02">
				<div class="p-inquiry-box">
					<div class="p-box-left">
						<div class="sp_none">
						<a href="http://n-techno.confirm-center-s.com/seminar/"><img class="../imghover" src="../images/top/img_top_24.png" alt="SEMINAR" /></a>
						</div>
						<div class="pc_none">
							<a href="http://n-techno.confirm-center-s.com/seminar/">
								<div class="p-text-left">
									<img src="../images/top/img_top_sp_06.png" alt="SEMINAR" />
								</div>
								<div class="p-text-right">
									<img src="../images/top/img_top_sp_07.png" alt="SEMINAR" />
								</div>
							</a>
						</div>
					</div>
					<div class="p-box-right">
						<a href="http://n-techno.confirm-center-s.com/internship/"><img class="imghover" src="../images/top/img_top_25.png" alt="INTERNSHIP" /></a>
					</div>
				</div>
			</div>
		</section>

  <!-- l-content --></div>


<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



