<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

<title>営業所一覧| RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報">
<link rel="canonical" href="#">

<!-- ページ共通のCSSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
<!-- ページ共通のCSSファイル終了-->

<!-- ページ共通のJSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
<!-- ページ共通のJSファイル終了-->

<!-- ページ固有のCSSファイル開始-->
<link rel="stylesheet" type="text/css" href="../css/base.css?v=1" media="all">
<!-- ページ固有のCSSファイル終了-->

<!-- ページ固有のJSファイル開始-->
<script type="text/javascript" src="js/top.js?v=1"></script>
<!-- ページ固有のJSファイル終了-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

<div class="l-pageBody">

	 <nav class="l-topicPath">
    <ol itemscope itemtype="http://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/employee/">
        <span itemprop="name">COMPANY</span></a>
        <meta itemprop="position" content="1" />
      </li>
      <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        ＞<a itemprop="item" href="/employee/voice01.html">
        <span itemprop="name">営業所一覧</span></a>
        <meta itemprop="position" content="2" />
      </li>
    </ol>
  </nav>
  <div class="l-content">
		<section class="p-base mt40">
				<div class="l-wrap">
        <h2 class="tac mb30"><img class="mauto" src="../images/base/img_base_01.png" class="sp_none" alt="営業所一覧" /></h2>

        <h2 class="tac mb30"><img class="mauto" src="../images/base/img_base_02.png" class="sp_none" alt="営業所マップ" /></h2>
        </div>
	</section>

  <section class="mb80">
    <div class="l-wrap">
    <div class="ml20 clearfix p-base-text">
      <div class="w25p flL">
        <p class="fs18 mb30">本社<br />
        ソーラーパワービル<br />
        テクノ・サテライト・オフィス</p>

        <ul>北海道支店
          <li>札幌営業所</li>
          <li>苫小牧営業所</li></ul>
        <ul>東北支店
          <li>秋田営業所</li>
          <li>盛岡営業所</li>
          <li>仙台営業所</li>
          <li>郡山営業所</li></ul>
        <ul>北関東支店
          <li>宇都宮営業所</li>
          <li>小山営業所</li>
          <li>高崎営業所</li></ul>
        <ul>首都圏支店
          <li>第一／第二／第三係</li>
          <li>第四／第五／第六係</li></ul>
        <ul>さいたま支店
          <li>さいたま第一／第二営業所</li>
          <li>西埼玉営業所</li>
          <li>熊谷営業所</li></ul>
      </div>

      <div class="w25p flL">
        <ul>信越支店
          <li>新潟営業所</li>
          <li>長岡営業所</li>
          <li>甲府営業所</li>
          <li>長野営業所</li>
          <li>松本営業所</li></ul>
        <ul>千葉茨城支店
          <li>つくば営業所</li>
          <li>水戸営業所</li>
          <li>柏営業所</li>
          <li>千葉第一／第二営業所</li></ul>
        <ul>圏央支店
          <li>立川営業所</li>
          <li>相模原営業所</li>
          <li>横浜第一／第二営業所</li>
          <li>藤沢営業所</li>
          <li>横須賀営業所</li></ul>
        <ul>中部支店
          <li>沼津営業所</li>
          <li>静岡営業所</li>
          <li>浜松営業所</li>
          <li>名古屋営業所</li>
          <li>岡崎営業所</li>
          <li>岐阜営業所</li>
          <li>三重営業所</li></ul>
      </div>

      <div class="w25p flL">
        <ul>関西支店
          <li>金沢営業所</li>
          <li>京都営業所</li>
          <li>滋賀営業所</li>
          <li>大阪第一／第二営業所</li>
          <li>堺営業所</li>
          <li>姫路営業所</li>
          <li>神戸営業所</li></ul>
        <ul>中四国支店
          <li>岡山営業所</li>
          <li>広島第一／第二営業所</li>
          <li>高松営業所</li>
          <li>松山営業所</li>
          <li>島根営業所</li>
          <li>山口営業所</li></ul>
        <ul>九州支店
          <li>北九州営業所</li>
          <li>福岡第一／第二営業所</li>
          <li>長崎営業所</li>
          <li>大分営業所</li>
          <li>熊本営業所</li>
          <li>鹿児島営業所</li></ul>
        <ul>沖縄支店
          <li>沖縄営業所</li></ul>
      </div>

      <div class="w25p flL">
         <ul>サービスセンター
            <li>旭川サービスセンター</li>
            <li>函館サービスセンター</li>
            <li>土浦サービスセンター</li>
            <li>足利サービスセンター</li>
            <li>さいたまサービスセンター</li>
            <li>千葉サービスセンター</li>
            <li>東京サービスセンター</li>
            <li>名古屋サービスセンター</li>
            <li>富山サービスセンター</li>
            <li>大阪サービスセンター</li>
            <li>四国中央サービスセンター</li>
            <li>和歌山サービスセンター</li>
            <li>福岡サービスセンター</li>
            <li>宮崎サービスセンター</li></ul>
      </div>
    </div>
    </div>
  </section>

  <section class="p-inquiry">
      <div class="l-wrap-02">
        <div class="p-inquiry-box">
          <div class="p-box-left">
            <div class="sp_none">
            <a href="#"><img class="imghover" src="../images/top/img_top_24.png" alt="SEMINAR" /></a>
            </div>
            <div class="pc_none">
              <a href="#">
                <div class="p-text-left">
                  <img src="../images/top/img_top_sp_06.png" alt="SEMINAR" />
                </div>
                <div class="p-text-right">
                  <img src="../images/top/img_top_sp_07.png" alt="SEMINAR" />
                </div>
              </a>
            </div>
          </div>
          <div class="p-box-right">
            <a href="#"><img class="imghover" src="../images/top/img_top_25.png" alt="INTERNSHIP" /></a>
          </div>
        </div>
      </div>
    </section>
	<!-- l-content --></div>

<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



