<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

<title>インターンシップ | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報">
<link rel="canonical" href="#">

<!-- ページ共通のCSSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
<!-- ページ共通のCSSファイル終了-->

<!-- ページ共通のJSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
<!-- ページ共通のJSファイル終了-->

<!-- ページ固有のCSSファイル開始-->
<link rel="stylesheet" type="text/css" href="../css/internship.css?v=1" media="all">
<!-- ページ固有のCSSファイル終了-->

<!-- ページ固有のJSファイル開始-->
<!-- ページ固有のJSファイル終了-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

<div class="l-pageBody">

	<nav class="l-topicPath">
		<ol itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="../">
				<span itemprop="name">RECRUIT</span></a>
				<meta itemprop="position" content="1" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				＞<a itemprop="item" href="../internship/">
				<span itemprop="name">インターンシップ</span></a>
				<meta itemprop="position" content="2" />
			</li>
		</ol>
	</nav>

	<div class="l-content">
            <div class="p-internship-top-img">
                <h2>
                	<img src="../images/internship/intern_out_01.png" class="sp_none" alt="インターンシップ" />
                    <img src="../images/internship/intern_out_01_sp.png" class="pc_none" alt="インターンシップ" />
                </h2>
       <!-- p-internship-top-img --></div>
    <div class="p-internship-inner">
        <h3>冬季インターンシップ<br class="pc_none" />について</h3>
            <h4>開催日</h4>
                <p>
                ①２０１７年２月１０日（金）９：３０－１６：３０<br />
                ②２０１７年２月１７日（金）９：３０－１６：３０
                </p>
                <h4>開催期間</h4>
                <p>１ＤＡＹ</p>
                <h4>定員</h4>
                <p>１０～１５名</p>
    <!-- p-internship-inner --></div>

    <div class="p-internship-inner2">
    <h3>*インターンシップ内容*</h3>
    <p>
    ＡＭ：省エネ業界・事業内容等説明<br />
	ＰＭ：グループワーク<br />
    <span>グループごとに当日提示する課題に取組んでいただき、最後にはプレゼンをしていただきます。</span>
    </p>

    <div class="p-notes">
    ※ＡＭ・ＰＭともに途中休憩がございます。<br />
	※昼食は当社にてお弁当をご用意いたします。<br />
	※エントリー画面よりエントリーをお願いいたします。
    </div>
    <div class="p-internship-recruit">
              <a href="https://job.mynavi.jp/18/pc/search/corp91114/outline.html?_vsm=qucoxjf0SEslr7-KnNMOdkNx4HK9vNlIgFv-G-9oFyCuCNU1Iz06IQzOcmHMl7eFmEWy3ZNAKTEYfYVXiQiH6GY9Q0wuvkHA1BAfnuNLwqbBN5jGD7vS43nFHZQBquGXQsLl60F4aezlM7rOIKVU2xa_HWUABlWlG24rkSzbppAzF4FO4-ujsz23OwO9T5vn9v3Boi1AyxDlDdVXeVTewUPhZOhFfiuYjF_rwsEsD7Kr-fR9MVOuophDP6Zfc1O7"><img class="imghover" src="../images/internship/intern_out_02.png" alt="マイナビ2018" /></a>
              <a href="https://job.rikunabi.com/2018/company/r968220076"><img class="imghover" src="../images/internship/intern_out_03.png" alt="リクナビ2018 プレエントリーはこちら" /></a>
          </div>
      <!-- p-internship-inner2 --></div>
	<!-- l-content --></div>


<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



