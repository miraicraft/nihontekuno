<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

<title>研修内容 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報">
<link rel="canonical" href="#">

<!-- ページ共通のCSSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
<!-- ページ共通のCSSファイル終了-->

<!-- ページ共通のJSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
<!-- ページ共通のJSファイル終了-->

<!-- ページ固有のCSSファイル開始-->
<link rel="stylesheet" type="text/css" href="../css/content.css?v=1" media="all">
<!-- ページ固有のCSSファイル終了-->

<!-- ページ固有のJSファイル開始-->
<!-- ページ固有のJSファイル終了-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

<div class="l-pageBody">

	<nav class="l-topicPath">
		<ol itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="../">
				<span itemprop="name">RECRUIT</span></a>
				<meta itemprop="position" content="1" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				＞<a itemprop="item" href="../training/">
				<span itemprop="name">研修・制度紹介</span></a>
				<meta itemprop="position" content="2" />
			</li>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				＞<a itemprop="item" href="../content/">
				<span itemprop="name">研修内容</span></a>
				<meta itemprop="position" content="3" />
			</li>

	</ol>
	</nav>

	<div class="l-content">
        <section class="p-content-top-img">
            <h2><img class="sp_none" src="../images/content/content_out_01.png" alt="研修内容" /><img class="pc_none" src="../images/content/content_out_01_sp.png" alt="研修内容" /></h2>
        </section>
        <div class="p-training-content">
        <h3 class="p-training-content-heading">研修内容</h3>
         <h4>入社前研修（2日間～1週間）：全職種</h4>
            <p class="p-border-bottom">

                先輩社員に付いて現場にて同行研修を行います。お客様との商談の様子や、先輩の営業トークを実際に見て聞いて学んでください。
            </p>

        <h4>新人研修（2週間）：全職種</h4>
            <p>

                仲間意識やライバル意識も芽生え、厳しい中にも楽しさとやりがいを感じることができる研修です。
            </p>
        <div class="p-trainign-border">
           <h5>知識研修</h5>
            <p>
             「電気基礎」・「電気保安」の講義をはじめ、わかりやすく解説しますので、電気の知識が全くない方でもご安心ください。そのほか、ビジネスマナーの復習や自社知識などの講義を行います。実際に製品を手にとって製品知識を学べますので、より理解を深めることができます。
            </p>
        <img src="../images/content/content_out_02.png" alt="知識研修" />
		<!-- p-trainign-border --></div>
         <div class="p-trainign-border">
            <h5>営業研修</h5>
            <p>
                当社の製品を売るためのトークを学ぶ実践型の研修です。研修内容は、基本的なマナーの復習、営業部長によるセールススキル研修などです。研修が終わるころには自信を持ってお客様に提案できるスキルを身につけることができます。また、当社トップセールスマンの営業トークを見ることもできます。そのほか同期生とロールプレイングをしたり、実践型の研修が中心ですので確実に実力をつけることができます。
                </p>
        <img class="p-sales" src="../images/content/content_out_03.png" alt="営業研修" />
		<!-- p-trainign-border --></div>
            <h4>社外研修（管理者養成学校）：全職種</h4>
            <p class="p-border-bottom">
                   静岡県の研修施設で泊まり込みで行う研修です。自ら考え発言する自主性を育成したり、ビジネスパーソンの基本的な姿勢や考え方を習得する場になりますので、社会人としての基礎を得ることができます。
            </p>

            <h4>沖縄研修（2週間）</h4>
            <p class="p-border-bottom">

              沖縄コールセンターにて行うテレアポ研修です。この研修では、専用の電話機を使って、1日に数百件の企業にアプローチしていきます。たくさんの企業との接点を持つことによって、今後の幅が広がります。
			</p>

            <!-- p-training-content --></div>

	<div class="p-training-content02">
   	  <h2 class="p-training-content-heading">研修の流れ</h2>
            <section class="p-training-flow">
                     <h3><img src="../images/content/content_out_04.png" alt="入社前研修" /></h3>
                     <p>同行研修</p>
        	</section>
            <div class="p-training-img100">
       	    <img class="sp_none" src="../images/content/content_out_05.png" alt="入社" />
             <img class="pc_none" src="../images/content/content_out_05_sp.png" alt="入社" />
            </div>
            <section class="p-training-flow">
                     <h3><img src="../images/content/content_out_06.png" alt="社外研修" /></h3>
                     <p>社外での泊まり込み研修</p>
        	</section>
            <section class="p-training-flow fs10">
                     <h3><img src="../images/content/content_out_07.png" alt="新人研修" /></h3>
                     自社知識・ビジネスマナー・業界関連・電気知識<br class="sp_none" />
                     営業研修・ロールプレイング
        	</section>
            <section class="p-training-flow">
                     <h3><img src="../images/content/content_out_08.png" alt="配属" /></h3>
                     <p>配属先営業所にて営業活動</p>
        	</section>
            <section class="p-training-flow">
                     <h3><img src="../images/content/content_out_09.png" alt="沖縄研修" /></h3>
                     <p>テレフォンアポイントメント研修</p>
        	</section>
            <section class="p-training-flow">
                     <h3><img src="../images/content/content_out_10.png" alt="フォローアップ研修" /></h3>
                     <p>営業活動の振り返り・基本トークのロールプレイング</p>
        	</section>
	<!-- p-training-content02 -->
    </div>

    	<div class="p-inquiry">
		  <div class="l-wrap-02">
				<div class="p-inquiry-box">
					<div class="p-box-left">
						<div class="sp_none">
						<a href="http://n-techno.confirm-center-s.com/seminar/"><img class="imghover" src="../images/top/img_top_24.png" alt="SEMINAR" /></a>
						</div>
						<div class="pc_none">
							<a href="http://n-techno.confirm-center-s.com/seminar/">
								<div class="p-text-left">
									<img src="../images/top/img_top_sp_06.png" alt="SEMINAR" />
								</div>
								<div class="p-text-right">
									<img src="../images/top/img_top_sp_07.png" alt="SEMINAR" />
								</div>
							</a>
						</div>
					</div>
					<div class="p-box-right">
						<a href="http://n-techno.confirm-center-s.com/internship/"><img class="imghover" src="../images/top/img_top_25.png" alt="INTERNSHIP" /></a>
					</div>
				</div>
			</div>
		</div>
	<!-- l-content --></div>


<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



