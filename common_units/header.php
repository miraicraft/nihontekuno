
<header id="pagetop" class="l-header">
	<div class="l-header-inner">
		<div class="clearfix">
			<h1><a href="<?php echo $root_directory;?>/"><img class="sp_none" src="<?php echo $root_directory;?>/images/common/header_logo.png" alt="日本テクノのロゴ"><img class="pc_none ml10 pt10" src="<?php echo $root_directory;?>/images/common/header_logo_sp.png" alt="日本テクノのロゴ">
			</a></h1>
			<nav class="l-gNav">
				<div class="sp_none">
					<ul>
						<li><a href="<?php echo $root_directory;?>/message/"><img class="imghover" src="<?php echo $root_directory;?>/images/common/img_navi_01.png" alt="MESSAGE" /></a></li>
						<li class="l-sub-nav"><img src="<?php echo $root_directory;?>/images/common/img_navi_02.png" alt="COMPANY" />
							<ul>
								<li><a href="<?php echo $root_directory;?>/profile/">会社概要</a></li>
								<li><a href="<?php echo $root_directory;?>/information/">事業案内</a></li>
								<li><a href="<?php echo $root_directory;?>/base/">拠点一覧</a></li>
							</ul>
						</li>
						<li class="l-sub-nav"><img src="<?php echo $root_directory;?>/images/common/img_navi_03.png" alt="MEMBER" />
							<ul>
								<li><a href="<?php echo $root_directory;?>/employee/">社員紹介</a></li>
								<li><a href="<?php echo $root_directory;?>/employee/">社員の声</a></li>
							</ul>
						</li>
						<li class="l-sub-nav"><img src="<?php echo $root_directory;?>/images/common/img_navi_04.png" alt="RECRUIT" />
							<ul>
								<li><a href="<?php echo $root_directory;?>/career/">職種紹介</a></li>
								<li><a href="<?php echo $root_directory;?>/requirements/">募集要項</a></li>
								<li><a href="<?php echo $root_directory;?>/training/">研修・精度紹介</a></li>
								<li><a href="<?php echo $root_directory;?>/seminar/">セミナー</a></li>
								<li><a href="<?php echo $root_directory;?>/internship/">インターンシップ</a></li>
							</ul>
						</li>
					</ul>
					<div class="l-entry-btn">
						<a href="<?php echo $root_directory;?>#"><img src="<?php echo $root_directory;?>/images/common/btn_entry_01.png" alt="entry" /></a>
					</div>
				</div>
				<div class="pc_none">
						<div class="clearfix">
							<div class="l-open-btn flR mr10">
								<img src="<?php echo $root_directory;?>/images/common/btn_menu_sp.png" alt="menu" />
							</div>
							<div class="flR mr10">
								<a href="<?php echo $root_directory;?>"><img src="<?php echo $root_directory;?>/images/common/btn_mypage_sp.png" alt="mypage" /></a>
							</div>
						</div>
				</div>
			</nav>
		</div>
	</div>
<div class="l-open-menu">
	<div class="l-inner">
		<ul class="clearfix">
			<li><a href="/message/"><img src="<?php echo $root_directory;?>/images/common/img_navi_sp_01.png" alt="社長メッセージ" width="100" /></a></li>
			<li><a href="/seminar/"><img src="<?php echo $root_directory;?>/images/common/img_navi_sp_02.png" alt="セミナー" width="100" /></a></li>
			<li><a href="/profile/"><img src="<?php echo $root_directory;?>/images/common/img_navi_sp_03.png" alt="会社概要" width="100" /></a></li>
			<li><a href="/information/"><img src="<?php echo $root_directory;?>/images/common/img_navi_sp_04.png" alt="事業案内" width="100" /></a></li>
			<li><a href="/employee/"><img src="<?php echo $root_directory;?>/images/common/img_navi_sp_05.png" alt="社員紹介" width="100" /></a></li>
			<li><a href="/questionnaire/"><img src="<?php echo $root_directory;?>/images/common/img_navi_sp_06.png" alt="社員アンケート" width="100" /></a></li>
			<li><a href="/seminar/"><img src="<?php echo $root_directory;?>/images/common/img_navi_sp_07.png" alt="ENTRY" width="100%"  /></a></li>
			<li><a href="/internship/"><img src="<?php echo $root_directory;?>/images/common/img_navi_sp_08.png" alt="INTERNSHIP" width="100%"  /></a></li>
		</ul>
	</div>
	<div class="l-close-btn">
		<img src="<?php echo $root_directory;?>/images/common/img_navi_sp_close.png" alt="閉じる" />
	</div>
	<div class="l-menu-bg"></div>
</div>
<!-- l-header --></header>
