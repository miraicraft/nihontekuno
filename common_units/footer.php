<footer class="l-footer"><div class="l-footer-inner">
<div class="sp_none">
	<div class="l-footer-links">
		<div class="clearfix">
			<ul>
				<li><a href="<?php echo $root_directory;?>/">HOME</a></li>
			</ul>
			<ul>
				<li><a href="<?php echo $root_directory;?>/message/">MESSAGE</a></li>
			</ul>
			<ul>
				<li>COMPANY</li>
				<li><a href="<?php echo $root_directory;?>/profile/">会社概要</a></li>
				<li><a href="<?php echo $root_directory;?>/information/">事業案内</a></li>
			</ul>
			<ul>
				<li>MEMBER</li>
				<li><a href="<?php echo $root_directory;?>/employee/">新卒社員紹介</a></li>
				<li><a href="<?php echo $root_directory;?>/employee/">新卒社員アンケート</a></li>
				<li><a href="<?php echo $root_directory;?>/employee/">女性社員の活躍</a></li>
			</ul>
			<ul>
				<li>RECRUIT</li>
				<li><a href="<?php echo $root_directory;?>/career/">職種紹介</a></li>
				<li><a href="<?php echo $root_directory;?>/requirements/">募集要項</a></li>
				<li><a href="<?php echo $root_directory;?>/training/">研修・精度紹介</a></li>
				<li><a href="<?php echo $root_directory;?>/seminar/">セミナー情報</a></li>
			</ul>
		</div>
	<!-- l-footer-links --></div>
	<div class="clearfix">
		<div class="flL"><a href="http://www.n-techno.co.jp/" target="_blank"><img class="imghover" src="<?php echo $root_directory;?>/images/common/footer_logo.png" alt="日本テクノのロゴ" /></a></div>
		<div class="flR pt20"><img src="<?php echo $root_directory;?>/images/common/img_copy.png" alt="Copyright NIHON TECHNO CO.LTD All Rights Reserved." /></div>
	</div>
</div>
<div class="pc_none">
	<div class="l-footer-links">
		<a href="#pagetop"><img src="<?php echo $root_directory;?>/images/common/icn_arrow_03.png" alt="topへ戻る" /></a>
		<a class="pb5" href="http://www.n-techno.co.jp/" target="_blank">コーポレートサイトへ</a>
		<a class="pb5" href="https://www.facebook.com/nihontechno.saiyo">新卒採用 facebook</a>
	</div>
</div>
</div><!-- l-footer --></footer>
