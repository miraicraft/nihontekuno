<?php
	global $root_directory ;
	$root_directory  = empty($_SERVER["HTTPS"]) ? "http://" : "https://";
	$root_directory .= $_SERVER['HTTP_HOST'];
?>

<meta charset="utf-8">
<!--[if lt IE 9]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->

<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width, minimal-ui" id="viewport">
<meta name="format-detection" content="telephone=no">