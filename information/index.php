<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

<title>会社概要| RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報">
<link rel="canonical" href="#">

<!-- ページ共通のCSSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
<!-- ページ共通のCSSファイル終了-->

<!-- ページ共通のJSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
<!-- ページ共通のJSファイル終了-->

<!-- ページ固有のCSSファイル開始-->
<link rel="stylesheet" type="text/css" href="../css/information.css?v=1" media="all">
<!-- ページ固有のCSSファイル終了-->

<!-- ページ固有のJSファイル開始-->
<!-- ページ固有のJSファイル終了-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

<div class="l-pageBody">

	<nav class="l-topicPath">
		<ol itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="/employee/">
				<span itemprop="name">COMPANY</span></a>
				<meta itemprop="position" content="1" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				＞<a itemprop="item" href="/employee/voice01.html">
				<span itemprop="name">事業案内</span></a>
				<meta itemprop="position" content="2" />
			</li>
		</ol>
	</nav>

	<div class="l-content">
	<section class="p-info">
		<div class="p-info-top-img">
        <h2 class="tac"><img src="../images/information/img_information_01.png" alt="会社紹介" /></h2>

     </div>
	</section>
    <section class="mb30">
    	<div class="l-wrap">
				<h3 class="color-blue tac">日本テクノは、お客さまと省エネルギー活動を通じて<br />
						地球環境問題に取り組んでいます</h3>
    	</div>
		</section>

		<section class="mb30">

		<div class="l-wrap">
			<div class="p-info-text">私たちの暮らしと密接に関わっている省エネは、企業や工場、
			店舗などビジネスの場でも大きな課題となっています。<br />
			日本テクノでは、電気の「見える化」「理解（わか）る化」によって企業の省エネ活動をサポート。<br />
			『SMARTMETER ERIA』『SMART CLOCK』といった自社開発システムの導入、
			電力コンサルティングのアフターフォローを行います。<br />
			また、受託数業界トップクラスの電気設備の保安管理、全国9エリアでの電力小売事業、
			テナントビル自動検針システム、省エネ設備改善、電気工事など、電気に関する総合的なサービスを提供。<br />
			発電事業においては、2012年には国内最大規模11万kW級の天然ガスエンジン発電所
			「日本テクノ袖ケ浦グリーンパワー」、2013年には「いばらき太陽光発電所」の運転を開始。<br />
			2015年12月には新潟県上越市にも天然ガスエンジン発電所「上越グリーンパワー」の運転を開始するなど、
			さらなる事業拡大を見据えています。<br />
			 電気のトータルソリューションカンパニーとして、企業の電気に、環境に、
			社会へさらなる貢献を目指していきます。</div>
		</div>
		</section>

		<section class="mb80">
    	<div class="l-wrap">
				<h3 class="color-blue tac">日本テクノは、お客さまと省エネルギー活動を通じて<br />
						地球環境問題に取り組んでいます</h3>
    	</div>
		</section>

		<section>
			<div class="l-wrap">

				<div class="posi-set border-gray mb70 mv5 pb40">
				<div class="posi-senter"><div class="bg-circle"><p class="fs18 tac color-white">つくる</p></div></div>
				<div class="clearfix mt60">
				<div class="flLa mb20"><p class="color-blue p-info-boxtext">
					電力小売<br />
					Ｘ<br />
					発電事業</p>
				</div>
				<div class="ml30 mt10 flLa"><img src="../images/information/img_information_02.png" class="sp_none" alt="セミナー情報" /></div>
				<div class="flLa p-contentbox"><p class="p-info-text">特定規模電気事業者（新電力）である日本テクノ。
						多種多様な信頼できる電源から、独自の電力発電需
						給管理システムを駆使して、電力の安定供給を行っています。</p></div>

				</div>
				</div>

				<div class="posi-set border-gray mb70 mv5 pb40">
				<div class="posi-senter"><div class="bg-circle"><p class="fs18 tac color-white">まもる</p></div></div>
				<div class="clearfix mt60">
				<div class="flLa"><p class="color-blue p-info-boxtext">
					電気保安<br />
					Ｘ<br />
					電気工事</p>
				</div>
				<div class="ml30 mt10 flLa"><img src="../images/information/img_information_03.png" class="sp_none" alt="セミナー情報" /></div>
				<div class="flLa p-contentbox"><p class="p-info-text">
					国家資格を有する当社の電気管理技術者および、
					協力会・日電協所属の電気管理技術者、全国1700社
					以上の協力工事店によって安心で信頼性の高い
					保守・点検を行っています。</p></div>

				</div>
				</div>

				<div class="posi-set border-gray mb20 mv5 pb40">
				<div class="posi-senter"><div class="bg-circle2"><p class="fs18 tac color-white">賢く<br />つかう</p></div></div>
				<div class="clearfix mt60">
				<div class="flLa p-boxtext-2"><p class="color-blue fs26 lh50">電力コンサルティング</p><img src="../images/information/img_information_04.png" class="sp_none" alt="セミナー情報" />
				</div>
				<div class="mt10 flLa p-contentbox"><p class="p-info-text">
					SMARTMETER ERIA・SMART CLOCKによる電気の
					「見える化」を利用し、お客さまに最適な省エネ活動を
					ご提案。アフターサービスを第一にお客さまの電気使用量
					削減に貢献します。</p></div>
				</div>
				</div>

				<div class="border-gray mb20 mv5 pb40">
				<div class="clearfix mt30">
				<div class="flLa p-boxtext-2"><p class="color-blue fs26 lh50">省エネ設備改善</p><img src="../images/information/img_information_05.png" class="sp_none" alt="セミナー情報" />
				</div>
				<div class="mt10 flLa p-contentbox"><p class="p-info-text">
					お客さまの電力使用状況を調査し、
					使い方に合わせた最適な省エネ設備をご提案しています。</p></div>
				</div>
			</div>

				<div class="border-gray mb70 mv5 pb40">
				<div class="clearfix mt30">
				<div class="flLa p-boxtext-2"><p class="color-blue fs26 lh50">電気料金自動検針<br class="pc_none" />システムィング</p><img src="../images/information/img_information_06.png" class="sp_none" alt="セミナー情報" />
				</div>
				<div class="mt10 flLa p-contentbox"><p class="p-info-text">
					テナントビルにおけるオフィスの電気料金を明確にし、
					ビルオーナー様・管理会社の検針・請求・回収業務の
					負担を解消します。</p></div>
				</div>
				</div>

			</div>
		</section>
	<!-- l-content --></div>

<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



