<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

<title>募集要項 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。日本テクノ新卒研修に2017年新卒の学生をご招待！往復交通費全額支給！">
<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報">
<link rel="canonical" href="#">

<!-- ページ共通のCSSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
<!-- ページ共通のCSSファイル終了-->

<!-- ページ共通のJSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
<!-- ページ共通のJSファイル終了-->

<!-- ページ固有のCSSファイル開始-->
<link rel="stylesheet" type="text/css" href="../css/requirements.css?v=1" media="all">
<!-- ページ固有のCSSファイル終了-->

<!-- ページ固有のJSファイル開始-->
<!-- ページ固有のJSファイル終了-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

<div class="l-pageBody">

	<nav class="l-topicPath">
		<ol itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="/employee/">
				<span itemprop="name">RECRUIT</span></a>
				<meta itemprop="position" content="1" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				＞<a itemprop="item" href="/employee/voice01.html">
				<span itemprop="name">募集要項</span></a>
				<meta itemprop="position" content="2" />
			</li>
		</ol>
	</nav>

	<div class="l-content">
<div class="p-requirements-h2-img">
            <h2>
                <img src="../images/requirements/requirements_01.png" class="sp_none" alt="募集要項" />
                <img src="../images/requirements/requirements_01_sp.png" class="pc_none" alt="募集要項" />
            </h2>
           <!-- p-requirements-h2-img --></div>
           <div class="p-requirements-recruit">
           <img src="../images/requirements/requirements_02.png" alt="努力の大切さを知っている人に来てほしい" />
             <h3>努力の大切さを知っている人に来てほしい</h3>
                   <p>
                       当社の経営理念「最初から出来る人はいない、だから努力すれば私にも出来る。
                       夢に描いたことは、努力することで必ず実現する『夢・志・計画・実行・達成』」
                       に共感できる方をお待ちしています。
                        一生懸命取り組んだ末に何かを得たエピソード等があれば、ぜひ聞かせてください。
             </p>
                       <p>
                       選考では、人事・各部門担当者・役員とお会いいただく予定です。
                       色々な立場の社員と話すことで、当社を多面的に捉えられると思います。（人事係：採用担当）
                 </p>
          <!-- p-requirements-recruit --> </div>
     <div class="p-requirements-h2-img">
            <h2>
              <img src="../images/requirements/requirements_03.png" class="sp_none" alt="募集要項" />
              <img src="../images/requirements/requirements_03_sp.png" class="pc_none" alt="募集要項" />
            </h2>
           <!-- p-requirements-h2-img --></div>
           
          <div class="p-requirements-guideline">
              <a href="#eigyo"><img class="imghover" src="../images/requirements/requirements_04.png" alt="総合職 営業"/></a>
              <a href="#gijyutu"><img class="imghover" src="../images/requirements/requirements_05.png" alt="総合職 技術" /></a>
              <a href="#jim"><img class="imghover" src="../images/requirements/requirements_06.png" alt="総合職 事務" /></a>
          <h3 id="eigyo">総合職 営業</h3>
              <h4>募集条件</h4>
                  <p>
                      2017年3月に4年制大学・大学院を卒業・修了見込の方
                  </p>
              <h4>勤務地</h4>
                  <p>
                      全国の各営業所<br />
                      &lt;&#60;配属先候補&gt;&#62;東京、北海道、岩手、宮城、福島、栃木、群馬、茨城、埼玉、千葉、神奈川、長野、新潟、
                      石川、山梨、静岡、愛知、三重、岐阜、滋賀、京都、大阪、兵庫、 岡山、広島、島根、山口、
                      香川、愛媛、福岡、大分、長崎、熊本、鹿児島 
                </p>
            <h4>勤務時間</h4>
                  <p>
                      9：00～18：00(休憩60分）
                  </p>
              <h4>研修・教育制度</h4>
                  <p>
                      ビジネスマナー、電気の基礎知識、営業研修などを行い、これからの長いキャリアのベースとなるような、
                      知識や考え方を学んでいただければと思っています。
                  </p>
              <h4>勤務時間</h4>
                  <p>
                      9：00～18：00(休憩60分）
                  </p>
              <h4>研修・教育制度</h4>
                  <p>
                      ビジネスマナー、電気の基礎知識、営業研修などを行い、これからの長いキャリアのベースとなるような、
                      知識や考え方を学んでいただければと思っています。
                  </p>
         <h3 id="gijyutu">総合職 技術</h3>
             <h4>募集条件</h4>
                 <p>
                     2017年3月に4年制大学・大学院・専門学校を卒業・修了見込の方理系（電気・電子・工学科) 卒業見込みの方、
                     または電気主任技術者・電気工事士をお持ちの方尚可
                 </p>
             <h4>勤務地</h4>
                 <p>
                     全国各地のサービスセンターまたは営業所 
                 </p>
             <h4>研修・教育制度</h4>
                 <p>
                     ビジネスマナーや自社知識などの講義を中心に、電気の基礎知識や電気保安、
                     社会人としての基礎と業務スキルを習得します。 
                 </p>
         <h3 id="jim">総合職 事務</h3>
             <h4>募集条件</h4>
                 <p>
                     2017年3月に4年制大学・大学院を卒業・修了見込の方
                 </p>
             <h4>勤務地</h4>
                 <p>
                     沖縄 
                 </p>
             <h4>研修・教育制度</h4>
                 <p>
                     ビジネスマナーや自社知識などの講義を中心に、電気の基礎知識や自社商品、
                     社会人としての基礎と 業務スキルを習得します。
                 </p>
        <!--  p-requirements-guideline --></div>
        
      <div class="p-requirements-h2-img">
            <h2>
              <img src="../images/requirements/requirements_07.png" class="sp_none" alt="採用までの流れ" />
              <img src="../images/requirements/requirements_07_sp.png" class="pc_none" alt="採用までの流れ" />
            </h2>
           <!-- p-requirements-h2-img --></div>
           
           <div class="p-requirements-flow">
               <h3>
               <img class="sp_none" src="../images/requirements/requirements_08.png" alt="採用までの流れ" />
               <img class="pc_none" src="../images/requirements/requirements_08_sp.png" alt="採用までの流れ" />
               </h3>
               <p>※募集職種によって異なる場合があります。</p>
      <!-- p-requirements-flow --></div>
           
      <div class="p-requirements-h2-img">
            <h2>
              <img src="../images/requirements/requirements_09.png" class="sp_none" alt="よくある質問" />
              <img src="../images/requirements/requirements_09_sp.png" class="pc_none" alt="よくある質問" />
            </h2>
           <!-- p-requirements-h2-img --></div>
       
           <div class="p-requirements-faq">
               <h3>職場環境について</h3>
                   <dl class="p-requirements-faq1">
                       <dt>平均年齢はどのくらいですか?</dt>
                           <dd>約36歳です。</dd>
                       <dt>男女比を教えてください。</dt>
                           <dd>営業本部は430名中、女性社員は現在38名です。（2016年2月現在）</dd>
                       <dt>社内の雰囲気（社風）を教えてください。</dt>
                           <dd>
                               バイタリティがあって向上心の強い社員が多いように感じます。<br />
                                雰囲気は、明るく元気で、たまに元気すぎるくらいです。 
                            </dd>
                       <dt>残業はどのくらいしていますか?</dt>
                           <dd>約30時間となっております。</dd>
                       <dt>先輩社員に相談できる環境ですか?</dt>
                           <dd>はい。先輩、上司が近くにいる席ですので、不明な点などすぐに確認できる環境です。</dd>
                       <dt>新卒の離職率はいくつですか？</dt>
                           <dd>16％となっています。（2016年2月現在）</dd>
                        <dt>産休や育児休暇の制度はありますか？</dt>
                           <dd>あります。現在取得中の方は、全国で25名です。（2016年2月現在） </dd>
                        <dt>女性が働きやすい職場ですか</dt>
                           <dd>
                           多くの女性が活躍しています。最近とくに産休や育休を取得している社員が多いです。<br />
                            営業部はまだまだ男性中心ですが、女性営業社員もバリバリ活躍中です！ </dd>
                        <dt>どんな社員が多いですか？</dt>
                           <dd>ポジティブで元気な人が多いです。</dd>
                   </dl>
               
               <h3>担当業務について</h3>
                   <dl>
                       <dt>営業マンの1日のスケジュールを教えてください。</dt>
                           <dd>
                               営業所で朝礼を行い、今日訪問予定の企業の対策を考え、上長とロールプレイングをしたり
                               、資料を作成し、10時頃から外出。17時くらいに帰社し、見積書を作成したり、事務作業を行い、
                               20時頃に帰宅します。
                           </dd>
                       <dt>どの様な営業活動を行いますか？</dt>
                           <dd>
                               ユーザー様へのアフターフォロー営業、省エネ活動の結果や、効果検証を報告します。
                               また、ユーザー様からご紹介企業を頂いた際は、その企業へ新規営業を行います。
                           </dd>
                       <dt>転勤はありますか？</dt>
                           <dd>
                               あります。転勤のタイミングは、新規開設拠点の戦力スタッフとしての異動、<br />
                                営業所長として栄転異動などです。 
                            </dd>
                       </dl>
                   
               <h3>選考について</h3>
                   <dl>
                       <dt>どのような学生を求めていますか?</dt>
                           <dd>
                               向上心があり、元気な方を求めています。
                           </dd>
                       </dl>

               
      <!-- p-requirements-faq --></div>
	<!-- l-content --></div>


<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



