<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

<title>セミナー | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。日本テクノ新卒研修に2017年新卒の学生をご招待！往復交通費全額支給！">
<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報">
<link rel="canonical" href="#">

<!-- ページ共通のCSSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
<!-- ページ共通のCSSファイル終了-->

<!-- ページ共通のJSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
<!-- ページ共通のJSファイル終了-->

<!-- ページ固有のCSSファイル開始-->
<link rel="stylesheet" type="text/css" href="../css/seminar.css?v=1" media="all">
<!-- ページ固有のCSSファイル終了-->

<!-- ページ固有のJSファイル開始-->
<!-- ページ固有のJSファイル終了-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

<div class="l-pageBody">

	<nav class="l-topicPath">
		<ol itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="../">
				<span itemprop="name">RECRUIT</span></a>
				<meta itemprop="position" content="1" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				＞<a itemprop="item" href="../seminar/">
				<span itemprop="name">セミナー情報</span></a>
				<meta itemprop="position" content="2" />
			</li>
		</ol>
	</nav>

	<div class="l-content">
        <div class="p-seminar-top-img">
        <h2><img src="../images/seminar/seminar_01.png" class="sp_none" alt="セミナー情報" /><img src="../images/seminar/seminar_01_sp.png" class="pc_none" alt="セミナー情報" /></h2>
        <p>現時点で決定しているセミナースケジュールを公開しています。<br>
        選考に進むためには、自社セミナーへの参加が必須となりますので、マイナビまたはリクナビよりお申し込みください。
        </p>
       <!-- p-seminar-top-img --></div>
           <div class="p-seminar-inner">
           <table>
                  <tr>
                    <th>日時</th>
                    <th>セミナー名称</th>
                    <th>開催地</th>
                    <th>会場</th>
                  </tr>
                  <tr>
                    <td>2月20日（水）<br />10：00～12：00</td>
                    <td>自社セミナー（営業職）</td>
                    <td>東京</td>
                    <td>新宿センタービル本社</td>
                  </tr>
                  <tr>
                    <td>2月23日（水）<br />10：00～12：00</td>
                    <td>自社セミナー（営業職）</td>
                    <td>東京</td>
                    <td>新宿センタービル本社</td>
                  </tr>
                  <tr>
                    <td>2月23日（水）<br />10：00～12：00</td>
                    <td>自社セミナー（営業職）</td>
                    <td>東京</td>
                    <td>新宿センタービル本社</td>
                  </tr>
            </table>


          <div class="p-seminar-recruit">
              <a href="https://job.mynavi.jp/18/pc/search/corp91114/outline.html?_vsm=qucoxjf0SEslr7-KnNMOdkNx4HK9vNlIgFv-G-9oFyCuCNU1Iz06IQzOcmHMl7eFmEWy3ZNAKTEYfYVXiQiH6GY9Q0wuvkHA1BAfnuNLwqbBN5jGD7vS43nFHZQBquGXQsLl60F4aezlM7rOIKVU2xa_HWUABlWlG24rkSzbppAzF4FO4-ujsz23OwO9T5vn9v3Boi1AyxDlDdVXeVTewUPhZOhFfiuYjF_rwsEsD7Kr-fR9MVOuophDP6Zfc1O7"><img class="imghover" src="../images/seminar/seminar_02.png" alt="マイナビ2018" /></a>
              <a href="https://job.rikunabi.com/2018/company/r968220076"><img class="imghover" src="../images/seminar/seminar_03.png"alt="リクナビ2018 プレエントリーはこちら" /></a>
          </div>
           <div class="p-seminar-internship">
           <h3><img class="sp_none" src="../images/seminar/seminar_04.png" alt="新卒研修ご招待" /><img class="pc_none" src="../images/seminar/seminar_04_sp.png" alt="新卒研修ご招待" /></h3>
           <div class="p-border-none">
           日本テクノでは新卒社員を対象としたさまざまな研修を行っています。<br />今回、そのひとつである代表取締役社長 馬本英一によるセミナーを2018年新卒の学生向けに一般公開することとなりました。<br />
日本テクノのこと、電力業界のこと、今と未来をやさしく説明します。<br />
開催は、2月を予定しておりますので、詳細が決まり次第こちらのページにて発表いたします。<br />
 みなさまのご参加をお待ちしております。
           </div>
           <h4>開催概要</h4>
              <h5>日時</h5>
                  <p>
                       2017年2月20日（月）、21日（火）、22日（水）<br />
                       10:00～16:10（途中休憩あり）　※開場：9:00<br />
                       ※各日とも同じ内容となりますので、ご都合の良い日時にてお申込みください。<br />
                       ※当日の進行状況により、終了時刻が前後する場合があります。
               </p>
            <h5>会場</h5>
                <p>

                       日本テクノ株式会社　本社セミナールーム<br />
                       〒163-0651　東京都新宿区西新宿1-25-1　新宿センタービル51階<br />
                       ○往復交通費全額支給（※1）

               </p>
           <h5>内容</h5>
                <p>

                       日本テクノの歴史と、電力業界の未来<br />
               </p>
            <h5>対象</h5>
               <p>

                       2017年3月に大学院、大学、短大、専門学校を卒業見込みの方<br />
                        短大、専門学校の方は、技術職（電気工事士、電気主任技術者）を志望の方のみ<br />
                        （※2018年3月に上記卒業見込みの方もお待ちしています）
               </p>
           <h5>持参</h5>
                <p>

                       学生証、印鑑来<br />
               </p>
            <h5>お問い合わせ</h5>
           <p>

                       日本テクノ株式会社 人事課 採用係<br />
                       0120-308-412（フリーコール）<br />
						※受付時間は9：00～18：00（土・日・祝日を除く）の未来
               </p>

            <h4>開催概要</h4>
            <p>下記の内容を、日本テクノ採用係宛にメールでお送りください。追って受付完了のご連絡を致します。</p>
             <h5>応募先</h5>
             <p>

                       n-techno@saiyo-g.net<br />
               </p>
           <h5>メールの件名</h5>
            <p>

                       日本テクノセミナー応募 <br />
               </p>
           <h5>メールの内容</h5>
            <p>

                       ①お名前②在学校名③ご連絡先④セミナー参加希望日（第二希望まで記載ください。） <br />
               </p>


          <div class="p-seminar-recruit2">
          <p>または、各ナビサイトより<br class="pc_none"/>エントリーしてください。</p>
              <a href="https://job.mynavi.jp/18/pc/search/corp91114/outline.html?_vsm=qucoxjf0SEslr7-KnNMOdkNx4HK9vNlIgFv-G-9oFyCuCNU1Iz06IQzOcmHMl7eFmEWy3ZNAKTEYfYVXiQiH6GY9Q0wuvkHA1BAfnuNLwqbBN5jGD7vS43nFHZQBquGXQsLl60F4aezlM7rOIKVU2xa_HWUABlWlG24rkSzbppAzF4FO4-ujsz23OwO9T5vn9v3Boi1AyxDlDdVXeVTewUPhZOhFfiuYjF_rwsEsD7Kr-fR9MVOuophDP6Zfc1O7"><img class="imghover" src="../images/seminar/seminar_02.png" alt="マイナビ2018" /></a>
              <a href="https://job.rikunabi.com/2018/company/r968220076"><img class="imghover" src="../images/seminar/seminar_03.png"alt="リクナビ2018 プレエントリーはこちら" /></a>
          </div>
          <div class="p-others">※会場の座席に限りがあるため、ご希望に添えない場合があることを、あらかじめご了承ください。</div>
             <div class="p-seminar-lecturer">
              <h4>講師紹介</h4>
              <h5>日本テクノ株式会社　代表取締役社長　馬本英一</h5>
              <p>
              東京都出身。学校法人Iwaki ヘアメイクアカデミーを卒業。理容室勤務を経て、セールスマンとしてさまざまな業種を経験する。1995 年、電気保安業務を主にする<br />
『日本テクノ株式会社』を設立。電気保安サービスに民間企業として初参入した。電気コンサルティングをはじめ、民間シェアNO.1の電気保安管理、電気小売り、電気工事、住宅省エネ化など、総合的なサービスを提供する。
              </p>
              <img src="../images/seminar/seminar_06.png" alt="講師：馬本英一" /></div>
           <!-- p-seminar-internship --></div>
           <div class="p-time-table">
           <h4>タイムスケジュール</h4>
           </div>
            <section class="p-seminar-flow">
                     <h3><img src="../images/seminar/seminar_out_02_01.png" alt="第1部" /></h3>
                     <p>社長講義 前半/代表取締役　馬本英一</p>
        	</section>
            <img class="sp_none" src="../images/seminar/seminar_out_02_02.png" alt="" />
            <img class="pc_none" src="../images/seminar/seminar_out_02_02_sp.png" alt="" />
             <section class="p-seminar-flow">
                     <h3><img src="../images/seminar/seminar_out_02_03.png" alt="第2部" /></h3>
                     <p>社長講義 後半/代表取締役　馬本英一</p>
        	</section>
            <section class="p-seminar-flow">
                     <h3><img src="../images/seminar/seminar_out_02_04.png" alt="第3部" /></h3>
                     <p>特別セミナー/日本テクノイメージキャラクター　河村隆一</p>
        	</section>
            <img class="sp_none" src="../images/seminar/seminar_out_02_05.png" alt=""/>
            <img class="pc_none" src="../images/seminar/seminar_out_02_05_sp.png" alt=""/>
<section class="p-seminar-flow">
      <h3><img src="../images/seminar/seminar_out_02_06.png" alt="第4部" /></h3>
                     <p>質疑応答、交通費支給</p>
        	</section>
                <div class="p-seminar-notes">
                <h4>【注意事項】</h4>
                ※1.セミナー会場（日本テクノ本社）への往復交通費は全額支給いたします。<br />
				当日精算のため、新幹線や飛行機でお越しの方は、往復分の領収書をご持参ください。<br />
				領収書がないと支給できません。<br />
				最短・最安値の交通ルートをご利用ください。<br />
				2.在来線、バスでお越しの方は領収書不要です。<br />
				但し、タクシー、車両関連費（レンタカー、ガソリン、駐車場）は支給できません。<br />
				3.交通費精算手続きのため、9：15までにご来場ください。<br />
				4.セミナー開始時間の5分前までに着席ください。セミナー開始後の途中入退出はできません。<br />
				5.お手洗い等は事前に済ませていただきますようお願いいたします。<br />
				6.来場の際はスーツでお越しください。<br />
				7.会場での食事、アルコール飲料、撮影、録音はご遠慮ください。<br />
				8.会場で迷惑行為をされる方は、退場いただく場合があります。
                </div>
      <!-- p-seminar-inner --></div>
	<!-- l-content --></div>


<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



