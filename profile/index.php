<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

<title>会社概要| RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報">
<link rel="canonical" href="#">

<!-- ページ共通のCSSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
<!-- ページ共通のCSSファイル終了-->

<!-- ページ共通のJSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
<!-- ページ共通のJSファイル終了-->

<!-- ページ固有のCSSファイル開始-->
<link rel="stylesheet" type="text/css" href="../css/profile.css?v=1" media="all">
<!-- ページ固有のCSSファイル終了-->

<!-- ページ固有のJSファイル開始-->
<!-- ページ固有のJSファイル終了-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

<div class="l-pageBody">

	<nav class="l-topicPath">
		<ol itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="/employee/">
				<span itemprop="name">COMPANY</span></a>
				<meta itemprop="position" content="1" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				＞<a itemprop="item" href="/employee/voice01.html">
				<span itemprop="name">会社紹介</span></a>
				<meta itemprop="position" content="2" />
			</li>
		</ol>
	</nav>
	<div class="l-content">

	<section class="p-profile">
				<div class="l-wrap">
        <h2 class="mb70 tac"><img class="mauto" src="../images/profile/img_profile_01.png" alt="飽くなき挑戦心が新しい未来を切り開くてく" /></h2>
        </div>
	</section>

		<section>
			<div class="l-wrap">
					<div class="clearfix mb40">
						<div class="flL w50p">
							<p class="p-profile-text mb30">
								日本テクノは、電力マネジメントの総合サービスを通じて、省エネルギー、地球環境保全に取り組む電気のトータルソリューションカンパニーです。<br />
								主力事業の電力コンサルティングでは、自社開発商品『SMARTMETER ERIA』『SMARTMETER CLOCK』によって、製造業やサービス業など幅広い事業者様の省エネルギーに貢献。<br />
								省エネ活動を定着させるアフターフォローサービス「GIFT123」によって、企業内に意識改善が行われることで、無理のない電気料金の削減を実現しています。<br />
								電力小売事業では全国9エリアで電力を供給し、電気保安管理業務は、受託数が全国で50,000件を超え、業界トップクラスを誇っています。
							</p>
						</div>
						<div class="flL mauto">
							<img src="../images/profile/img_profile_02.png" class="sp_none" alt="時計の画像" />
						</div>
					</div>
			</div>
		</section>

		<section>
				<div class="l-wrap">
				<p class="color-black p-profile-text mb30 ">　その他、発電事業やテナントビル自動検針、設備改善による省エネ対策など、電力サービスを川上から川下まで展開。<br />
				設立20年で売上高460億円を超える企業規模に発展し、BtoB分野では実力派ベンチャー企業として知られる存在となっています。<br />
 					創業以来当社が変わらず持ち続けているのは、常に新しいことへ挑戦するフロンティアスピリット。<br />
					近年では、BEMSアグリゲーターとして多くの事業者様とともに電力使用量の削減や電力ピーク対策に取り組み、全国で展開する電力小売事業での実績や国内最大規模（※）の天然ガスエンジン発電所の運営などによって、新電力としても存在感を発揮。</p>

					<div class="mb40 tac"><img class="mauto" src="../images/profile/img_profile_03.png" class="sp_none" alt="発電所の画像" /></div>

					<p class="color-black p-profile-text mb30 ">2015年12月には新潟県上越市に当社2基目となる天然ガスエンジン発電所を新たに稼働させるなど、総合電力企業としての基盤をより強固にしています。<br />
					当社には、コツコツと前向きに努力できる人材が活躍できるフィールドがあり、年齢や社歴に関係なく成果が正当に評価されるフェアな人事考課のもと、新卒入社から「3年後には営業所長として営業所をマネジメントする」といったスピード感のあるキャリアを描くことが可能です。<br />
					自分の未来は自分で切り開いていきたい── そんな成長意欲のある方とお会いできることを楽しみにしています。</p>

					<p class="color-black p-profile-text mb30 ">※　出典元：日刊工業新聞（2011年9月29日掲載）</p>
				</div>
		</section>

		<section class="p-company">
		<div class="p-company-detail-img">
        <h2 class="tac"><img src="../images/profile/img_profile_04.png" alt="会社概要" /></h2>

     </div>
    </section>
    <section class="mb30">
		<div class="l-wrap">
			<div class="p-profile-text">
			<dl class="bb-gray">
			<dt>商号</dt>

			<dd>日本テクノ株式会社</dd>
			</dl>
			<dl class="bb-gray">
			<dt>本社所在地</dt>
			<dd>東京都新宿区西新宿1-25-1 新宿センタービル51階</dd>

			</dl>
			<dl class="bb-gray">
			<dt>本社電話番号</dt>
			<dd>03-3349-1111</dd>
			</dl>
			<dl class="bb-gray">
			<dt>設立</dt>
			<dd>1995年4月4日</dd>
			</dl>
			<dl class="bb-gray">
			<dt>資本金</dt>
			<dd>5億7,194万円</dd>
			</dl>
			<dl class="bb-gray">
			<dt>従業員</dt>
			<dd>1031名（2016年3月1日現在）</dd>
			</dl>
			<dl class="bb-gray">
			<dt>売上高</dt>
			<dd>438億円(2014年12月期)</dd>
			</dl>
			<dl class="bb-gray">
			<dt>事業所</dt>
			<dd><span class="inline-block">【本社・本社営業部】</span>
					<span class="inline-block">新宿センタービル51F</span><br />
					<span class="inline-block">【ソーラーパワービル】</span>
					<span class="inline-block">神奈川県相模原市</span><br />
					<span class="inline-block">【テクノ・サテライト・オフィス】</span>
					<span class="inline-block">沖縄</span><br />
					<span class="inline-block">【営業所】</span>
					<span class="inline-block">札幌、</span><span class="inline-block">苫小牧、</span><span class="inline-block">秋田、</span><span class="inline-block">盛岡、</span><span class="inline-block">仙台、</span><span class="inline-block">郡山、</span><span class="inline-block">宇都宮、</span><span class="inline-block">小山、</span><span class="inline-block">高崎、</span><span class="inline-block">長岡、</span><span class="inline-block">水戸、</span><span class="inline-block">つくば、</span><span class="inline-block">熊谷、</span><span class="inline-block">さいたま、</span><span class="inline-block">西埼玉、</span><span class="inline-block">立川、</span><span class="inline-block">新宿、</span><span class="inline-block">柏、</span><span class="inline-block">千葉、</span><span class="inline-block">横浜、</span><span class="inline-block">藤沢、</span><span class="inline-block">相模原、</span><span class="inline-block">横須賀、</span><span class="inline-block">新潟、</span><span class="inline-block">長野、</span><span class="inline-block">松本、</span><span class="inline-block">金沢、</span><span class="inline-block">甲府、</span><span class="inline-block">沼津、</span><span class="inline-block">静岡、</span><span class="inline-block">浜松、</span><span class="inline-block">名古屋、</span><span class="inline-block">岡崎、</span><span class="inline-block">三重、</span><span class="inline-block">岐阜、</span><span class="inline-block">金沢、</span><span class="inline-block">滋賀、</span><span class="inline-block">京都、</span><span class="inline-block">大阪、</span><span class="inline-block">堺、</span><span class="inline-block">姫路、</span><span class="inline-block">神戸、</span><span class="inline-block">岡山、</span><span class="inline-block">広島、</span><span class="inline-block">島根、</span><span class="inline-block">山口、</span><span class="inline-block">高松、</span><span class="inline-block">松山、</span><span class="inline-block">北九州、</span><span class="inline-block">福岡、</span><span class="inline-block">長崎、</span><span class="inline-block">大分、</span><span class="inline-block">熊本、</span><span class="inline-block">鹿児島、</span><span class="inline-block">沖縄</span><span class="inline-block"></span><span class="inline-block"></span><br />
					<span class="inline-block">【サービスセンター】</span>
					<span class="inline-block">旭川、</span><span class="inline-block">函館、</span><span class="inline-block">足利、</span><span class="inline-block">土浦、</span><span class="inline-block">さいたま、</span><span class="inline-block">千葉、</span><span class="inline-block">東京、</span><span class="inline-block">名古屋、</span><span class="inline-block">富山、</span><span class="inline-block">大阪、</span><span class="inline-block">和歌山、</span><span class="inline-block"></span><span class="inline-block">四国中央、</span><span class="inline-block">福岡、</span><span class="inline-block">宮崎</span><br />
				</dd>
			</dl>
			<dl class="bb-gray">
			<dt>株主構成</dt>
			<dd>				馬本　英一（当社代表取締役社長）
				SMBCベンチャーキャピタル株式会社<br />
				エレクス株式会社<br />
				オリックス株式会社<br />
				株式会社さがみはら産業創造センター<br />
				東京海上日動火災保険株式会社<br />
				東邦電子株式会社<br />
				株式会社ビジョン<br />
				富士火災海上保険株式会社<br />
				株式会社三菱東京UFJ銀行<br />
				三菱UFJキャピタル株式会社<br />
				株式会社横浜銀行<br />
				りそなキャピタル株式会社<br />
				 （五十音順・敬称略）</dd>
			</dl>
			<dl class="bb-gray">
			<dt>主な取引先</dt>
			<dd>			オリックス株式会社<br />
			川崎重工業株式会社<br />
			株式会社クレディセゾン<br />
			鶴賀電機株式会社<br />
			東邦電子株式会社<br />
			三井住友ファイナンス＆リース株式会社<br />
			 三菱UFJリース株式会社<br />
			 （五十音順・敬称略）</dd>

			</dl>
			<dl class="bb-gray">
			<dt>許認可等</dt>
			<dd>登録電気工事業者登録票　経済産業大臣登録第25011号</dd>

			</dl>
			<dl class="bb-gray">
			<dt>関連会社</dt>
			<dd>			日本テクノパワー株式会社（100％子会社）<br />
			日本テクノエンジ株式会社（100％子会社）<br />
			株式会社フェスコパワーステーション滋賀（100%子会社）<br />
			株式会社ファーストエスコ（コード番号：9514 東証第一部）</dd>
			</dl>
			<dl class="bb-gray">
			<dt>業務提携</dt>
			<dd>電気管理技術者　1030名(日本テクノ協力会・日電協) ※2016年3月1日現在</dd>
			</dl>
			</div>
		</div>
		</section>

		<section class="mb100">
			<div class="l-wrap">
			<div class="p-content-title">
			<h2 class="pl10 fs18 color-white">業績</h2>
			</div>

				<div class="mt30 tac"><img class="mauto" src="../images/profile/img_profile_05.png" class="sp_none" alt="発電所の画像" /></div>
			</div>
		</section>
	<!-- l-content --></div>

<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



