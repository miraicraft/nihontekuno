<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

<title>社長メッセージ| RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイト。日本テクノ代表から2018年新卒の皆様へ向けたメッセージです。">
<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報">
<link rel="canonical" href="#">

<!-- ページ共通のCSSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
<!-- ページ共通のCSSファイル終了-->

<!-- ページ共通のJSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
<!-- ページ共通のJSファイル終了-->

<!-- ページ固有のCSSファイル開始-->
<link rel="stylesheet" type="text/css" href="../css/message.css?v=1" media="all">
<!-- ページ固有のCSSファイル終了-->

<!-- ページ固有のJSファイル開始-->
<!-- ページ固有のJSファイル終了-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

<div class="l-pageBody">

	<nav class="l-topicPath">
		<ol itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="/employee/">
				<span itemprop="name">MESSAGE</span></a>
				<meta itemprop="position" content="1" />
			</li>
		</ol>
	</nav>

	<div class="l-content">
	<section class="p-message">
		<div class="p-seminar-top-img">
        <h2 class="tac"><img src="../images/message/img_message_01.png" alt="セミナー情報" /></h2>

     </div>
		</section>

		<section class="p-president">
			<div class="l-wrap">
				<h3 class="p-president-img"><img src="../images/message/img_message_02.png" alt="RECRUIT" /></h3>
			</div>
		</section>

		<section>
			<div class="l-wrap">
				<div class="p-message-text">
						<p class="mb30">日本テクノを設立してからの日々は挑戦の繰り返しです。<br />
						ときには壁にぶつかることもありましたが、常に真摯にお客さまと向き合い、その壁を乗り越えてきたことが、今では大きな経験となり財産となっています。<br />
						事業の出発点は、すべてが「人の役に立ちたい」という思いから。<br />
						一部の特定法人に独占されていた電気保安業界に、民間企業として参入を果たしたのも、その思いに突き動かされてのことでした。</p>

						<p class="mb30">最初から出来る人はいない、だから努力すれば私にも出来る」<br />
						この日本テクノの経営理念は、綺麗事や月並みな励ましではありません。私を含め社員全員が実践してきたことです。<br />
						ゼロから立ち上げた事業が2004年1月の電気事業法改正につながる波を起こし、今では電気保安法人として50,000件を超える事業所様から信頼される業界トップクラスの企業へと飛躍を遂げています。<br />
						2016年からは電力自由化が本格化するなど、電力業界はこれから大きく変わっていきます。<br />
						そして刻々と変化する社会情勢や経済状況にも、当社は電気保安管理業務を基盤にお客さまへのアフターフォローに全力を注ぎ、そしてサービスや商品、組織も柔軟に変化させていくことで、日本一のカスタマーサービス企業を目指していきます。<br />
						当社の躍進に理由があるとすれば、常に真摯に努力を怠らなかったこと。<br />
						未来を見据えチャレンジを忘れないベンチャー精神は、これからも変えずに持ち続けていきます。<br />
						「夢に描いたことは、努力することで必ず実現する」<br />
						あなたにだって必ずできる。<br />
						日本テクノはあなたの挑戦を待っています。</p>
			</div>
			</div>
		</section>

		<section class="p-inquiry">
			<div class="l-wrap-02">
				<div class="p-inquiry-box">
					<div class="p-box-left">
						<div class="sp_none">
						<a href="#"><img class="imghover" src="../images/top/img_top_24.png" alt="SEMINAR" /></a>
						</div>
						<div class="pc_none">
							<a href="#">
								<div class="p-text-left">
									<img src="../images/top/img_top_sp_06.png" alt="SEMINAR" />
								</div>
								<div class="p-text-right">
									<img src="../images/top/img_top_sp_07.png" alt="SEMINAR" />
								</div>
							</a>
						</div>
					</div>
					<div class="p-box-right">
						<a href="#"><img class="imghover" src="../images/top/img_top_25.png" alt="INTERNSHIP" /></a>
					</div>
				</div>
			</div>
		</section>
	<!-- l-content --></div>

<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



