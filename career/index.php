<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

  <title>職種紹介 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
  <meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイト。募集職種の仕事内容をご紹介します。">
  <meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報">
  <link rel="canonical" href="#">

  <!-- ページ共通のCSSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
  <!-- ページ共通のCSSファイル終了-->

  <!-- ページ共通のJSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
  <!-- ページ共通のJSファイル終了-->

  <!-- ページ固有のCSSファイル開始-->
  <link rel="stylesheet" href="/css/career.css">
  <!-- ページ固有のCSSファイル終了-->

  <!-- ページ固有のJSファイル開始-->
  <script type="text/javascript">
    function smartRollover() {
      if(document.getElementsByTagName) {
        var images = document.getElementsByTagName("img");
        for(var i=0; i < images.length; i++) {
          if(images[i].getAttribute("src").match("_off."))
          {
            images[i].onmouseover = function() {
              this.setAttribute("src", this.getAttribute("src").replace("_off.", "_on."));
            }
            images[i].onmouseout = function() {
              this.setAttribute("src", this.getAttribute("src").replace("_on.", "_off."));
            }
          }
        }
      }
    }
    if(window.addEventListener) {
      window.addEventListener("load", smartRollover, false);
    }
    else if(window.attachEvent) {
      window.attachEvent("onload", smartRollover);
    }

    $(function(){
       // #で始まるアンカーをクリックした場合に処理
       $('a[href^=#]').click(function() {
          // スクロールの速度
          var speed = 400; // ミリ秒
          // アンカーの値取得
          var href= $(this).attr("href");
          // 移動先を取得
          var target = $(href == "#" || href == "" ? 'html' : href);
          // 移動先を数値で取得
          var position = target.offset().top;
          // スムーススクロール
          $('body,html').animate({scrollTop:position}, speed, 'swing');
          return false;
        });
     });
   </script>
   <!-- ページ固有のJSファイル終了-->

   <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
 </head>

 <body id="pagetop">
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

  <div class="l-pageBody">

    <nav class="l-topicPath">
      <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="./">
            <span itemprop="name">職種紹介</span></a>
            <meta itemprop="position" content="1" />
          </li>
        </ol>
      </nav>

      <div class="l-content">
        <!-- l-content -->
        <section class="p-career">
          <h2 class="p-bd tac"><img class="sp-w65p" src="../images/career/title.png" alt="WORK TYPE 職種紹介"></h2>
          <nav class="anchor">
            <ul>
              <li><a href="#p-type01"><img class="imghover" src="../images/career/anchor-bnr_01.png" alt="総合職 営業"></a></li>
              <li><a href="#p-type02"><img class="imghover" src="../images/career/anchor-bnr_02.png" alt="総合職 技術"></a></li>
              <li><a href="#p-type03"><img class="imghover" src="../images/career/anchor-bnr_03.png" alt="総合職 事務（沖縄勤務）"></a></li>
            </ul>
          </nav>
          <section id="p-type01" class="mt70 sp-mt20">
            <h3 class="p-blue_bg">総合職&emsp;営業</h3>
            <p class="p-txt">工場、商業施設、福祉施設やオフィスビルなど、電気使用の多い法人のお客様へ、自社開発商品<br>「SMARTMETER ERIA」「SMART CLOCK」の提案や省エネコンサルティングを行います。<br>省エネ対策の計画・実行に関するアフターフォローや電力小売りの提案、シェア拡大に向けた営業活動全般をご担当いただきます。</p>
            <div class="p-type01-flow">
              <ol class="sp_none">
                <li>
                  <p><img src="../images/career/type01_img_01.jpg" alt="アポイント"></p>
                  <ul class="p-blue">
                    <li>担当エリアへ架電</li>
                  </ul>
                </li>
                <li>
                  <p><img src="../images/career/type01_img_02.jpg" alt="提案"></p>
                  <ul class="p-blue">
                    <li>事業サービス内容の<br>ご案内</li>
                    <li>見積書</li>
                  </ul>
                </li>
                <li>
                  <p><img src="../images/career/type01_img_03.jpg" alt="契約"></p>
                  <ul class="p-blue">
                    <li>書類作成から手続き</li>
                  </ul>
                </li>
                <li>
                  <p><img src="../images/career/type01_img_04.jpg" alt="アフター"></p>
                  <ul class="p-blue">
                    <li>定期訪問</li>
                    <li>改善提案</li>
                    <li>導入効果の報告</li>
                  </ul>
                </li>
              </ol>
              <ol class="pc_none">
                <li>
                  <p class="p-flow-icon"><img src="../images/career/sp_type01_img_01.jpg" alt="アポイント"></p>
                  <p class="p-flow-title-blue">アポイント</p>
                  <ul class="p-blue">
                    <li>担当エリアへ架電</li>
                  </ul>
                </li>
                <li>
                  <p class="p-flow-icon"><img src="../images/career/sp_type01_img_02.jpg" alt="提案"></p>
                  <p class="p-flow-title-blue">提案</p>
                  <ul class="p-blue">
                    <li>事業サービス内容のご案内</li>
                    <li>見積書</li>
                  </ul>
                </li>
                <li>
                  <p class="p-flow-icon"><img src="../images/career/sp_type01_img_03.jpg" alt="契約"></p>
                  <p class="p-flow-title-blue">契約</p>
                  <ul class="p-blue">
                    <li>書類作成から手続き</li>
                  </ul>
                </li>
                <li>
                  <p class="p-flow-icon"><img src="../images/career/sp_type01_img_04.jpg" alt="アフター"></p>
                  <p class="p-flow-title-blue">アフター</p>
                  <ul class="p-blue">
                    <li>定期訪問</li>
                    <li>改善提案</li>
                    <li>導入効果の報告</li>
                  </ul>
                </li>
              </ol>
            </div>
          </section>
          <section id="p-type02" class="mt70 sp-mt20">
            <h3 class="p-blue_bg">総合職&emsp;技術</h3>
            <p class="p-txt">高圧受変電設備（キュービクル）へ、24時間監視装置「ESシステム」をはじめとする各種商品の設置工事や補助業務を行います。事業場の現場調査から工事日の調整、配線工事や設置後の計量信号（パルス）の接続などが主な業務です。<br>また、工事で使用する備品の補充などの在庫管理もご担当いただきます。 </p>
            <div class="p-type02-flow">
              <ol class="sp_none">
                <li>
                  <p><img src="../images/career/type02_img_01.jpg" alt="現場調査"></p>
                  <ul class="p-green">
                    <li>現場設備の確認</li>
                  </ul>
                </li>
                <li>
                  <p><img src="../images/career/type02_img_02.jpg" alt="設置"></p>
                  <ul class="p-green">
                    <li>スケジュールの確認</li>
                    <li>接続確認</li>
                  </ul>
                </li>
                <li>
                  <p><img src="../images/career/type02_img_03.jpg" alt="保安"></p>
                  <ul class="p-orange">
                    <li>点検業務</li>
                    <li>技術セミナー</li>
                  </ul>
                </li>
              </ol>
              <ol class="pc_none">
                <li>
                  <p class="p-flow-icon"><img src="../images/career/sp_type02_img_01.jpg" alt="現場調査"></p>
                  <p class="p-flow-title-green">現場調査</p>
                  <ul class="p-green">
                    <li>現場設備の確認</li>
                  </ul>
                </li>
                <li>
                  <p class="p-flow-icon"><img src="../images/career/sp_type02_img_02.jpg" alt="設置"></p>
                  <p class="p-flow-title-green">設置</p>
                  <ul class="p-green">
                    <li>スケジュールの確認</li>
                    <li>接続確認</li>
                  </ul>
                </li>
                <li>
                  <p class="p-flow-icon"><img src="../images/career/sp_type02_img_03.jpg" alt="保安"></p>
                  <p class="p-flow-title-orange">保安</p>
                  <ul class="p-orange">
                    <li>点検業務</li>
                    <li>技術セミナー</li>
                  </ul>
                </li>
              </ol>
            </div>
          </section>
          <section id="p-type03" class="mt70 sp-mt20">
            <h3 class="p-blue_bg">総合職&emsp;事務（沖縄勤務）</h3>
            <p class="p-txt">テクノ・サテライト・オフィス内でのコール業務、お客様情報の入力やデータ管理が主な業務です。<br>電話やメールでのお問い合わせ対応をはじめ、新規開拓のアポイントや省エネのアフターフォローなど、業務は配属部署により異なります。お客様と日本テクノをつなぐ総合事務をご担当いただきます。 </p>
            <div class="p-type03-flow">
              <ol class="sp_none">
                <li>
                  <p><img src="../images/career/type03_img_01.jpg" alt="コール"></p>
                  <ul class="p-red">
                    <li>[新規]お客様へ架電</li>
                  </ul>
                </li>
                <li>
                  <p><img src="../images/career/type03_img_02.jpg" alt="アフター"></p>
                  <ul class="p-red">
                    <li>[既存]お客様向け<br>お問い合わせ<br>アフターフォロー</li>
                  </ul>
                </li>
                <li>
                  <p><img src="../images/career/type03_img_03.jpg" alt="データ管理"></p>
                  <ul class="p-red">
                    <li>作成／管理／更新</li>
                  </ul>
                </li>
                <li>
                  <p><img src="../images/career/type03_img_04.jpg" alt="監視センター"></p>
                  <ul class="p-orange">
                    <li>常時設備を監視</li>
                    <li>緊急応動の手配</li>
                  </ul>
                </li>
              </ol>
              <ol class="pc_none">
                <li>
                  <p class="p-flow-icon"><img src="../images/career/sp_type03_img_01.jpg" alt="コール"></p>
                  <p class="p-flow-title-red">コール</p>
                  <ul class="p-red">
                    <li>[新規]お客様へ架電</li>
                  </ul>
                </li>
                <li>
                  <p class="p-flow-icon"><img src="../images/career/sp_type03_img_02.jpg" alt="アフター"></p>
                  <p class="p-flow-title-red">アフター</p>
                  <ul class="p-red">
                    <li>[既存]お客様向け<br>お問い合わせ<br>アフターフォロー</li>
                  </ul>
                </li>
                <li>
                  <p class="p-flow-icon"><img src="../images/career/sp_type03_img_03.jpg" alt="データ管理"></p>
                  <p class="p-flow-title-red">データ管理</p>
                  <ul class="p-red">
                    <li>作成／管理／更新</li>
                  </ul>
                </li>
                <li>
                  <p class="p-flow-icon"><img src="../images/career/sp_type03_img_04.jpg" alt="監視センター"></p>
                  <p class="p-flow-title">監視センター</p>
                  <ul class="p-orange">
                    <li>常時設備を監視</li>
                    <li>緊急応動の手配</li>
                  </ul>
                </li>
              </ol>
            </div>
          </section>
          <section>
            <h3 class="p-blue_bg">ワンストップサービス</h3>
            <p class="p-txt">電気に関する総合サービスをワンストップでお届けできるのが日本テクノの強みです。</p>
            <div class="p-onst-flow">
              <ul>
                <li><img src="../images/career/onst_img_01.jpg" alt="コール アポイント"></li>
                <li><img src="../images/career/onst_img_02.jpg" alt="提案"></li>
                <li><img src="../images/career/onst_img_03.jpg" alt="契約"></li>
                <li><img src="../images/career/onst_img_04.jpg" alt="現場調査"></li>
                <li><img src="../images/career/onst_img_05.jpg" alt="データ整理"></li>
                <li><img src="../images/career/onst_img_06.jpg" alt="設置"></li>
                <li><img src="../images/career/onst_img_07.jpg" alt="アフター"></li>
                <li><img src="../images/career/onst_img_08.jpg" alt="アフター"></li>
                <li><img src="../images/career/onst_img_09.jpg" alt="監視センター・保安"></li>
              </ul>
            </div>
          </section>
        </section>

        <section class="p-inquiry">
          <div class="l-wrap-02">
            <div class="p-inquiry-box">
              <div class="p-box-left">
                <div class="sp_none">
                  <a href="../seminar/"><img class="imghover" src="../images/career/seminar_bnr.png" alt="SEMINAR" style="opacity: 1;"></a>
                </div>
                <div class="pc_none">
                  <a href="../seminar/">
                    <div class="p-text-left">
                      <img src="../images/career/sp_seminar_bnr_01.png" alt="SEMINAR">
                    </div>
                    <div class="p-text-right">
                      <img src="../images/career/sp_seminar_bnr_02.png" alt="SEMINAR">
                    </div>
                  </a>
                </div>
              </div>
              <div class="p-box-right">
                <a href="../internship/"><img class="imghover" src="../images/career/intern_bnr.png" alt="INTERNSHIP" style="opacity: 1;"></a>
              </div>
            </div>
          </div>
        </section>
      </div>


      <!-- l-pageBody --></div>

      <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
      <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
    </body>
    <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
    </html>



