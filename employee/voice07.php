<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

  <title>池田 武司 | 社員紹介 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
  <meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
  <meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報,インタビュー,営業">
  <link rel="canonical" href="#">

  <!-- ページ共通のCSSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
  <!-- ページ共通のCSSファイル終了-->

  <!-- ページ共通のJSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
  <!-- ページ共通のJSファイル終了-->

  <!-- ページ固有のCSSファイル開始-->
  <link rel="stylesheet" href="../css/employee.css">
  <!-- ページ固有のCSSファイル終了-->

  <!-- ページ固有のJSファイル開始-->
  <!-- ページ固有のJSファイル終了-->

  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

  <div class="l-pageBody">

    <nav class="l-topicPath">
      <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="./">
            <span itemprop="name">社員紹介</span></a>
            <meta itemprop="position" content="1" />
          </li>
          <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            ＞<a itemprop="item" href="voice07.html">
            <span itemprop="name">池田　武司</span></a>
            <meta itemprop="position" content="2" />
          </li>
        </ol>
      </nav>

      <div class="l-content">
        <section class="p-voice07">
          <div class="p-mv">
            <h2><img src="../images/employee/voice07_mv_title.png" alt="IKEDA TAKESHI"></h2>
            <p class="p-sub-title mt35 sp-mt20"><img src="../images/employee/voice07_mv_txt.png" alt="鉄道会社と迷い、鉄道も電気がないと動かない。インフラも一番は電気であるとの認識から決意"></p>
            <div class="p-mv-box">
              <p>池田　武司<br>電力システム本部　電力事業部　電力取引課<br>学習院大学法学部卒　2013年新卒入社</p>
            </div>
          </div>

          <div class="p-voice-wrap">
            <div class="wrapper mt50">
              <section class="p-voice-cont">
                <h3><img src="../images/employee/voice_title_01.png" alt="入社のきっかけ"></h3>
                <p>2011年3月11日に東日本大震災があり、その後の計画停電も経験しました。このときに、インフラビジネスに興味を持ちました。そして、就職活動が始まり、練習のつもりで参加した会社説明会で強烈なインパクトを受け、さらに電気に携わる会社であることが決め手でした。最後まで悩んだのは、地元の鉄道会社でした。しかし、その鉄道も電気がないと動かない。<br>つまり、インフラでも一番は電気であるとの認識から最後は日本テクノに決めました。</p>
              </section>
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_02.png" alt="入社してみて感じたこと"></h3>
                <p>地元にはもう戻らないと思っていたのですが、配属先は地元である福岡営業所でした。高校時代の先輩や同期も多く、心強かったです。営業で厳しいことも多かったですが、社内外の方々に支えられ、順調に契約を上げることができました。3年以内に主任になり、電力事業部への異動希望を出そうと考えていましたが、入社1年半が経った2014年7月の定期人事異動のタイミングで、電力事業部へと異動となりました。<br>願いが早くかなったので、うれしかったです。ただ、異動してからが大変でした。電気のことを知らなさすぎる自分との葛藤が始まりました。営業でも話していたので、大丈夫だと思っていましたが、まるでわかっていませんでした。<br>「朝礼で、その日の新聞を読んで１つ質問をする」ことが本当に苦痛でした。最初は何をどう考えれば良いのかから始まって、理解できていることは何で、何がわからないのかを整理する毎日でした。今ではもっと知りたい、もっと理解したいとの思いが強く、新聞を読まないと一日が始まりません。電気以外のことも最近は理解できるようになり、社会のことや企業のことを興味深く読んでいます。</p>
              </section>
            </div>
            <section class="pb70">
              <h3 class="p-bd mt40"><img src="../images/employee/voice_title_08.png" alt="新卒社員の一日　ONEDAY"></h3>
              <ul class="p-timeline">
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>07:45　出社</h4>
                        <p class="p-txt">日本経済新聞、電気新聞を一読して、気になった記事はコピーをとり、熟読して整理していく。朝礼での質問を準備する時間でもあり、さらに自分自身の勉強の時間でもある。質問も的を射てなければ意味がない。<br>本人の理解度も図られるため、しっかりと整理することが大切である。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice07_img_01.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>09:00　朝礼</h4>
                    <p class="p-txt">JPEX（日本卸電力取引所）の動向解説が部長より行われ、<br>その後に本日の業務予定と質問となる。質問に対する回答は、課長や部長が行い、補足説明を副社長から直々に聞くことができる。ここでの理解と整理が日々の業務に活かされていく。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>09:15　お客様の登録や確認</h4>
                        <p class="p-txt">午前中の業務は、東京電力管内で新たに日本テクノから電力を供給するお客様の登録や確認。登録はインターネット上で行える。ベースになるデータはエクセルに入っているため、エクセルは使いこなせないと効率は悪くなる。書類の整理を行いながら、確実性を求められる仕事である。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice07_img_02.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>12:00　ランチ</h4>
                    <p class="p-txt">新宿センタービル内のレストランで。ちょっと値段は高いけど…</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>13:00</h4>
                    <p class="p-txt">午後は切りかえに伴うメーターの交換工事依頼をインターネット上で行い、さらに、電力供給の提案企業への連絡や供給開始の案内などを電話にて行う。条件を明確に伝え、供給後のトラブルを発生させないように丁寧に対応する。営業職時代から口下手で電話が苦手だったため、工夫していたのは、お客様から質問を引き出すようにすること。お客様が理解していない限り質問は出てこない。しっかりと聞き役に徹して対応を続ける。 </p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>13:45　郵送書類発送</h4>
                        <p class="p-txt">前日、先方より「じゃ、お願いするよ」と内諾をもらっていた会社。契約書への署名、捺印が完了したとの一報を受けて再度訪問した。すべての書類に不備がないことを確認し、無事契約に至った。 </p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice07_img_03.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>15:30　休憩</h4>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>15:45　電話業務</h4>
                    <p class="p-txt">苦手な電話業務だが懸命にかけ続ける。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>18:30　業務日報記入</h4>
                        <p class="p-txt">良かった点、悪かった点、今日の質問とその回答を明記して帰る。「自分に知識がないとこの部署でやっていけないという危機感はあります。今はひとつひとつの業務の意味や業界のことが少しでもわかることが楽しいです。<br>
                          少しずつでも前に進む自分の姿を追い求めていきます。もっともっと頑張ります！」</p>
                        </div>
                        <p class="flr sp-fln"><img src="../images/employee/voice07_img_04.jpg" alt=""></p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="p-timeline-content">
                      <h4>19:05　退社</h4>
                      <p class="p-txt">「本日もお疲れ様でした」</p>
                    </div>
                  </li>
                </ul>
              </section>
              <div class="wrapper pb70">
                <section class="p-voice-cont">
                  <h3><img src="../images/employee/voice_title_03.png" alt="仕事の息抜き法"></h3>
                  <p><img src="../images/employee/voice07_img_05.jpg" alt=""></p>
                  <p>コーヒーを飲むことと、軽くストレッチをすること。</p>
                </section>
                <section class="p-voice-cont sp-mt20">
                  <h3><img src="../images/employee/voice_title_04.png" alt="休日の過ごし方"></h3>
                  <p>小学生のころから剣道をしていて、今でも時々出身大学に顔を出しています。仕事帰りや休日にジムで体を動かすのもストレス解消法です。</p>
                </section>
              </div>
              <section>
              <h3 class="p-blue_bg">就活生への応援メッセージ</h3>
                <div class="wrapper">
                  <p class="p-mg-img mr40 sp-center sp-mt20"><img src="../images/employee/voice07_img_06.jpg" alt=""></p>
                  <p class="p-mg-txt">最後に迷ったら人で選べば間違いないと思います。出来るだけ多くの<br>人に会って自分に合う会社を見つけてください。</p>
                </div>
              </section>
            </div>
          </section>
          <section class="p-inquiry mt50">
            <div class="l-wrap-02">
              <div class="p-inquiry-box">
                <div class="p-box-left">
                  <div class="sp_none">
                    <a href="../seminar/"><img class="imghover" src="../images/employee/seminar_bnr.png" alt="SEMINAR" style="opacity: 1;"></a>
                  </div>
                  <div class="pc_none">
                    <a href="../seminar/">
                      <div class="p-text-left">
                        <img src="../images/employee/sp_seminar_bnr_01.png" alt="SEMINAR">
                      </div>
                      <div class="p-text-right">
                        <img src="../images/employee/sp_seminar_bnr_02.png" alt="SEMINAR">
                      </div>
                    </a>
                  </div>
                </div>
                <div class="p-box-right">
                  <a href="../internship/"><img class="imghover" src="../images/employee/intern_bnr.png" alt="INTERNSHIP" style="opacity: 1;"></a>
                </div>
              </div>
            </div>
          </section>
          <!-- l-content --></div>


          <!-- l-pageBody --></div>

          <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
          <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
        </body>
        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
        </html>



