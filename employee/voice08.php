<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

  <title>中塚 理奈 | 社員紹介 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
  <meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
  <meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報,インタビュー,営業">
  <link rel="canonical" href="#">

  <!-- ページ共通のCSSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
  <!-- ページ共通のCSSファイル終了-->

  <!-- ページ共通のJSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
  <!-- ページ共通のJSファイル終了-->

  <!-- ページ固有のCSSファイル開始-->
  <link rel="stylesheet" href="../css/employee.css">
  <!-- ページ固有のCSSファイル終了-->

  <!-- ページ固有のJSファイル開始-->
  <!-- ページ固有のJSファイル終了-->

  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

  <div class="l-pageBody">

    <nav class="l-topicPath">
      <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="./">
            <span itemprop="name">社員紹介</span></a>
            <meta itemprop="position" content="1" />
          </li>
          <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            ＞<a itemprop="item" href="voice08.html">
            <span itemprop="name">池田　武司</span></a>
            <meta itemprop="position" content="2" />
          </li>
        </ol>
      </nav>

      <div class="l-content">
        <section class="p-voice08">
          <div class="p-mv">
            <h2><img src="../images/employee/voice08_mv_title.png" alt="NAKATSUKA RINA"></h2>
            <p class="p-sub-title mt35 sp-mt20"><img src="../images/employee/voice08_mv_txt.png" alt="「人」に共感して入社　後輩の指導を通して自分自身の成長を感じる日々"></p>
            <div class="p-mv-box">
              <p>中塚　理奈<br>東日本営業部　首都圏第一支店　第二係（取材当時）<br>首都圏支店　第五係（現在）日本大学文理学部体育学科卒　2012年新卒入社</p>
            </div>
          </div>

          <div class="p-voice-wrap">
            <div class="wrapper mt50">
              <section class="p-voice-cont">
                <h3><img src="../images/employee/voice_title_01.png" alt="入社のきっかけ"></h3>
                <p>　高校時代からハンドボールをはじめ、自分が好きなことをやろうと体育学科を選んで日本大学に入学しました。体育教師を目指す同級生が多い中、どの分野でも良いので「No.1」の会社に入りたいと就職サイトで検索して、日本テクノを知りました。<br>最終面接で副社長にお会いして入社を決めました。失礼な表現かもしれませんが、「素晴らしい方なのに面白い」と強烈な印象でした。人事の方も面倒見の良い方々ばかりで「人」に共感して入社しました。</p>
              </section>
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_02.png" alt="入社してみて感じたこと"></h3>
                <p>　「人」に対する印象は入社しても変わりませんでした。なかなか契約が取れずに悩んでいたときに、当時の所長から「苦労は必ずする。いつするかだよ」と言われ「今、苦労しているから、いつか楽になる」と信じて走り続けました。少しずつ出来ることが増えて楽しくなっていく一方で、プレッシャーが増していくのを感じています。<br>やりがいは大きいですが、楽になることはない。楽になったら成長が止まっていると考えるようになりました。後輩から相談をうけ、昔は自分も出来なかったことを指導しているときに、自身の成長を感じます。また、大学の同級生に比べても自分自身の成長を感じています。業種を問わずに訪問し、多くの経営者の方々との商談やアフターフォローで、様々なことを教えていただいている環境に感謝しています。</p>
              </section>
            </div>
            <section class="pb70">
              <h3 class="p-bd mt40"><img src="../images/employee/voice_title_08.png" alt="新卒社員の一日　ONEDAY"></h3>
              <ul class="p-timeline">
                <li>
                  <div class="p-timeline-content">
                    <h4>07:30　出社</h4>
                    <p class="p-txt">本日の訪問先への資料や契約書類を確認。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>08:45　朝礼</h4>
                        <p class="p-txt">昨日の報告と本日の目標を発表。所長より連絡事項と月末に向けての行動量に対する注意があった。<br>売上目標を達成するための行動量目標である。その行動量が未達成ということは仕事を放棄していることになる。しっかりとスケジュール調整をして行動目標を達成するようにと指摘を受けた。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice08_img_01.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>09:00　お客様へのアポイント設定や<br>本日の商談の最終確認やアドバイスを頂く</h4>
                        <p class="p-txt">行動量を大きく左右するアフターフォローのアポイントの予定をしっかりと立てていく。担当するエリア内の移動は自転車。行ったり来たりでロスのないように調整する。<br>10年以上のお付き合いのあるお客様への更新契約に向けたアフターフォロー内容と方針の最終確認を所長と行う。会社の成長やこれから目指していく姿をどうアピールするのかを考える。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice08_img_02.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>10:00　担当エリアに向けて出発</h4>
                        <p class="p-txt">電車に乗って担当エリアの駅まで向かう。乗り換え方法も急ぐときと提案内容を考える時間が必要なときで異なる。最寄駅に到着すると、自転車置き場へ向かう。代々受け継がれてきた自転車は、後ろに大きなボックス、前には営業カバンが横向きに入る大きなカゴ。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice08_img_03.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>10:40　A社に向けて出発</h4>
                    <p class="p-txt">坂道を極力避けるように移動。このエリアを担当して半年。<br>住宅街を抜ける裏道も覚えた。下り坂が気持ちよいと思ったのは、最初だけ。当たり前のことではあるが、いつも使う駅が高いところにあるので、下ると結局は上ることに気が付いた。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>11:00　A社訪問</h4>
                        <p class="p-txt">サックスを製造している楽器工場。１カ所が高圧で、住宅を挟んだもう１カ所が低圧であったが、真ん中の住宅を土地交換にて取得。3軒続きの土地になった。より効率良く電気を使いたいとの要望。これまでの経験で回答できることと出来ないことがあった。調べて早々に返答する。こちらには種別変更の提案を行う。アフターフォローの中で発覚した新事実があり、年間10万円弱の削減ができる。スムーズに進み、後は東京電力の確認というところまで商談を進める。 </p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice08_img_04.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>11:45</h4>
                        <p class="p-txt">A社を出て、コンビニエンスストアの前で休憩。と思ったら、事業場を全国に持つお客様より、ひとつの事業場を閉鎖することになったと連絡が入る。設置している監視装置をどうするのかを関係部署とやり取り。パソコンと携帯をフル活用して対応する </p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice08_img_05.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>12:10　ランチ</h4>
                    <p class="p-txt">コンビニエンスストアの前でお弁当を食べることもあるが、この日の暑さでは体力の消耗が激しいため、久しぶりにゆっくりと中華を食べる。暑いときは、ピリッと麻婆丼。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>13:00　B社到着　売電契約について</h4>
                        <p class="p-txt">お客様から質問が2点。日本テクノの供給体制は安定しているか？その他の新電力ではダメなのか？1点目は上越グリーンパワーが今冬に発電開始予定であることを伝えると納得いただけた。2点目は先方が自ら他の新電力に申し込んだところお断りを受けていた。今回の訪問で資料も揃ったので、先方に稟議書を上げてもらう段取りはすべてクリアした。月内にとの約束をして商談は終了。<br>その後は自社製品についての専門知識の習得方法などについて意見交換を行う。 </p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice08_img_06.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>13:40　B社退社</h4>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>14:00　C社訪問</h4>
                    <p class="p-txt">映像制作会社に半年毎のアフターフォローで訪問。確実に電力ピークは抑えていたが、つい先日の猛暑でピークを越えた。従業員の意識は確実に変わっていたが、作業環境を優先したいとの思いも理解できる。辛い省エネ活動では良くないとの判断である。導入前と比較すると確実に下がっているので、継続することを一番に考えて協力していく。ERIAモニターを確認しながらのアフターフォローとなる。 </p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>15:30　D社訪問</h4>
                    <p class="p-txt">売電契約を検討していただけ、本日は必要書類の説明に訪問。<br>工場長から本社に書類を渡すため、記入できる箇所はしっかりと記入して押印依頼をする。時間がないとのことであったが、日本経済について少し雑談。中国人観光客の爆買による景気上昇に騙されてはいけないとわかりやすく説明していただく。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>17:00　E社訪問</h4>
                        <p class="p-txt">再来年で3回目の契約更新となるお付き合いの長いお客様だが、不思議なほど前任者からの情報が少ない。いろいろとお話をする中でその理由が判明してきた。各事業場に環境対策の責任者がいて、省エネに関しては、運用だけではなく設備改善も自社で行っている。電気に関しても回路計測まで行っていた。今の日本テクノのアフターフォローを実感して頂かなくては、次回の更新契約にも影響するため対策を練る。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice08_img_07.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>17:45　E社退社</h4>
                    <p class="p-txt">長い登り坂を自転車で駅まで向かう。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>18:40　帰社</h4>
                    <p class="p-txt">本日の訪問の整理を行い、支店長と所長が会議に入っていたため、終礼を任される。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>19:00　終礼</h4>
                    <p class="p-txt">アフターフォローの実施状況の確認、月末に向けての契約見込みの共有を行う。個人毎に本日の成果と明日の予定および行動量目標の進捗状況を報告。報告を受けて、一人ひとりにあったアドバイスや出来ていることに対しての良かった点の確認を行い、所員全員と情報共有を図る。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>20:00　退社</h4>
                    <p class="p-txt">本日は午後から非常に厳しい残暑となった。明日に向けてゆっくりと休みます。お疲れ様でした。</p>
                  </div>
                </li>
              </ul>
            </section>
          </div>
        </section>
        <section class="p-inquiry mt50">
          <div class="l-wrap-02">
            <div class="p-inquiry-box">
              <div class="p-box-left">
                <div class="sp_none">
                  <a href="../seminar/"><img class="imghover" src="../images/employee/seminar_bnr.png" alt="SEMINAR" style="opacity: 1;"></a>
                </div>
                <div class="pc_none">
                  <a href="../seminar/">
                    <div class="p-text-left">
                      <img src="../images/employee/sp_seminar_bnr_01.png" alt="SEMINAR">
                    </div>
                    <div class="p-text-right">
                      <img src="../images/employee/sp_seminar_bnr_02.png" alt="SEMINAR">
                    </div>
                  </a>
                </div>
              </div>
              <div class="p-box-right">
                <a href="../internship/"><img class="imghover" src="../images/employee/intern_bnr.png" alt="INTERNSHIP" style="opacity: 1;"></a>
              </div>
            </div>
          </div>
        </section>
        <!-- l-content --></div>


        <!-- l-pageBody --></div>

        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
      </body>
      <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
      </html>



