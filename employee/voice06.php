<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

  <title>大西 夏子 | 社員紹介 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
  <meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
  <meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報,インタビュー,営業">
  <link rel="canonical" href="#">

  <!-- ページ共通のCSSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
  <!-- ページ共通のCSSファイル終了-->

  <!-- ページ共通のJSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
  <!-- ページ共通のJSファイル終了-->

  <!-- ページ固有のCSSファイル開始-->
  <link rel="stylesheet" href="../css/employee.css">
  <!-- ページ固有のCSSファイル終了-->

  <!-- ページ固有のJSファイル開始-->
  <!-- ページ固有のJSファイル終了-->

  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

  <div class="l-pageBody">

    <nav class="l-topicPath">
      <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="./">
            <span itemprop="name">社員紹介</span></a>
            <meta itemprop="position" content="1" />
          </li>
          <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            ＞<a itemprop="item" href="voice06.html">
            <span itemprop="name">大西 夏子</span></a>
            <meta itemprop="position" content="2" />
          </li>
        </ol>
      </nav>

      <div class="l-content">
        <section class="p-voice06">
          <div class="p-mv">
            <h2><img src="../images/employee/voice06_mv_title.png" alt="OONISHI NATSUKO"></h2>
            <p class="p-sub-title mt35 sp-mt20"><img src="../images/employee/voice06_mv_txt.png" alt="ひとつの判断、ひとつの行動が、自分の成長につながっています"></p>
            <div class="p-mv-box">
              <p>大西 夏子<br>保安本部　管理・教育担当<br>協力会管理課　申請係　係長2008年5月入社</p>
            </div>
          </div>

          <div class="p-voice-wrap">
            <div class="wrapper mt50">
              <section class="p-voice-cont">
                <h3><img src="../images/employee/voice_title_15.png" alt="パート主婦からの転身"></h3>
                <p>日本テクノに入社して8年になりますが、入社前は、2人の娘の子育てをしながらファミリーレストランで接客や調理の仕事をしていました。当時は子どもが小さかったので、子ども中心の生活を考えてパートで働いていました。その後、子育てが一段落し、正社員として復帰したいと考えるようになりました。自分自身も年齢が30歳を過ぎて、どこまでがんばれるのか、チャレンジしてみたいという気持ちになったのです。</p>
              </section>
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_16.png" alt="申請件数は月に1,300件！"></h3>
                <p>所属の係では、当社が提供している電気の保安管理サービスに関する経済産業省への申請業務を担当しています。本社には10名のメンバーがおり、新規に契約されたお客様はもちろん、既存のお客様の契約内容変更など申請手続きは全国で月に約1,300件。同じ部内には保安管理の専門知識をもった先輩方がたくさんいらっしゃるので、相談しやすい環境が整っており安心ですが、お客様からいただいたさまざまな情報が営業担当者を通して私たちにバトンタッチされ、最終的な申請書類として国に提出されるので、非常に重要な役割だと思っています。</p>
              </section>
            </div>
            <div class="wrapper mt20 sp-mt0">
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_17.png" alt="メンバー全員が成長できる環境を"></h3>
                <p>係のメンバーは女性が多いです。係長としてみんなをまとめる立場ですが、常に心がけているのは、全員で成長できる環境にしたいということ。日々いろいろな相談が寄せられますが、話をしっかりと聞いた上で、私が答えを出すのではなく、メンバー同士で相談することを促したり、「みんなで力を合わせて解決しようよ」というスタンスです。<br>手を差し伸べることは簡単ですが、あえて突き放すことで、一人ひとりが考えて行動する機会を多く作りたいと思っています。</p>
              </section>
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_18.png" alt="日本テクノってどんなところ？"></h3>
                <p><img src="../images/employee/voice06_img_01.jpg" alt=""></p>
                <p>会社全体としてはまだまだ男性社員のほうが多いですが、男女の違いで差別を感じたことは一度もありません。最近は社内制度として産休や育休も充実してきましたし、さらに女性の意見に耳を傾けてくれる環境が整ってきたと感じています。男性には男性の勇ましさがあって、女性には女性のやさしさがあって、それぞれの良いところを掛け合わせてみんなが気持ちよく働ける環境が理想ですよね。これからは男性社員にも積極的に育休を取ってほしいですし、介護休暇などの必要性も高まってくると思います。<br>私自身の「子育てと仕事の両立」の経験が、少しでもこれからの働きやすい環境づくりのきっかけになればうれしいです。</p>
              </section>
            </div>
            <div class="wrapper mt20 sp-mt0 pb70">
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_19.png" alt="リフレッシュは、ひとり立ち飲み！"></h3>
                <p class="p-mg-txt">休日は、仕事のことを忘れて頭と体をしっかりと休ませています。日本テクノに入社した当時に小学生だった2人の娘も今は20歳と19歳になり、一緒に朝までカラオケなんてことも。いい息抜きになっています。それから大事にしているのはひとりの時間。日帰りスパやエステなど自分の時間を大切に、ひとり焼肉、ひとり立ち飲み、気楽にどこにでも行って楽しんでいます。</p>
              </section>
            </div>
            <section>
              <h3 class="p-blue_bg">就活生のみなさんへ</h3>
              <p class="p-mg-img-r"><img src="../images/employee/voice06_img_02.jpg" alt=""></p>
              <p class="p-mg-txt ml20 sp-center sp-mt20">何か行動を起こすときには、自分のなかで何らかの判断をします。だからこれまで判断してきた回数や、行動してきた経験が今の自分をつくっていく。そう考えると自分で限界を決めては駄目。自分の道は自分で選んで、その選択に対して一生懸命やることで結果がついてくるのだと思います。それと仕事を一生懸命やるには、プライベートでも同じように夢中になれるものがないと面白くありませんよね。<br>私の場合は、それが子どもたちのとのカラオケや、ひとり立ち飲みかな。（笑）人それぞれですが、生活を楽しみながら、一緒に成長していける仲間が入社してくれることを楽しみにしています！ </p>
            </section>
          </div>
        </section>
        <section class="p-inquiry mt50">
          <div class="l-wrap-02">
            <div class="p-inquiry-box">
              <div class="p-box-left">
                <div class="sp_none">
                  <a href="../seminar/"><img class="imghover" src="../images/employee/seminar_bnr.png" alt="SEMINAR" style="opacity: 1;"></a>
                </div>
                <div class="pc_none">
                  <a href="../seminar/">
                    <div class="p-text-left">
                      <img src="../images/employee/sp_seminar_bnr_01.png" alt="SEMINAR">
                    </div>
                    <div class="p-text-right">
                      <img src="../images/employee/sp_seminar_bnr_02.png" alt="SEMINAR">
                    </div>
                  </a>
                </div>
              </div>
              <div class="p-box-right">
                <a href="../internship/"><img class="imghover" src="../images/employee/intern_bnr.png" alt="INTERNSHIP" style="opacity: 1;"></a>
              </div>
            </div>
          </div>
        </section>
        <!-- l-content --></div>


        <!-- l-pageBody --></div>

        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
      </body>
      <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
      </html>



