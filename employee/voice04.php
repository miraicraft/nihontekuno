<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

  <title>西川 愛子 | 社員紹介 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
  <meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
  <meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報,インタビュー,営業">
  <link rel="canonical" href="#">

  <!-- ページ共通のCSSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
  <!-- ページ共通のCSSファイル終了-->

  <!-- ページ共通のJSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
  <!-- ページ共通のJSファイル終了-->

  <!-- ページ固有のCSSファイル開始-->
  <link rel="stylesheet" href="../css/employee.css">
  <!-- ページ固有のCSSファイル終了-->

  <!-- ページ固有のJSファイル開始-->
  <!-- ページ固有のJSファイル終了-->

  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

  <div class="l-pageBody">

    <nav class="l-topicPath">
      <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="./">
            <span itemprop="name">社員紹介</span></a>
            <meta itemprop="position" content="1" />
          </li>
          <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            ＞<a itemprop="item" href="voice04.html">
            <span itemprop="name">西川　愛子</span></a>
            <meta itemprop="position" content="2" />
          </li>
        </ol>
      </nav>

      <div class="l-content">
        <section class="p-voice04">
          <div class="p-mv">
            <h2><img src="../images/employee/voice04_mv_title.png" alt="NISHIKAWA AIKO"></h2>
            <p class="p-sub-title mt35 sp-mt20"><img src="../images/employee/voice04_mv_txt.png" alt="周囲に支えられ、
             安心して制度を活用できた"></p>
             <div class="p-mv-box">
              <p>西川 愛子<br>営業企画部　ET・首都圏営業課<br>2007年新卒入社</p>
            </div>
          </div>

          <div class="p-voice-wrap">
            <div class="wrapper mt50">
              <section class="p-voice-cont">
                <h3><img src="../images/employee/voice_title_09.png" alt="仕事内容"></h3>
                <p>私の担当業務は、電気料金自動検針サービス『エコテナント』にご契約いただいた、ビルオーナー様と入居者様へ、電気工事の段取りとサービスを開始するまでのご案内や、それにかかる事務管理などです。<br>施工の現場担当者、テナント担当者と多角的な観点から施工内容を確定し、施工後はその後のサービス開始までをフォロー。<br>滞りなくサービスを開始していただけるように準備していきます。通常の勤務時間は9時から18時までですが、現在は9時30分から16時30分までの時短勤務をしています。</p>
              </section>
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_10.png" alt="産休・育休制度について"></h3>
                <p><img src="../images/employee/voice04_img_01.jpg" alt=""></p>
                <p>自社商品の設置が主な仕事です。正しく設置されないと会社として正しいサービスが提供できないので、安全に丁寧に設置しています。横浜市を中心にESシステム、ERIA、SMART CLOCKだけではなく、エコテナントやLVMの設置もしています。特にエコテナントの設置は土曜、日曜が多いですが、代休はしっかりと取れます。休みの日は、第1種電気工事士の資格取得に向けて勉強しています。<br>くの経験を積んでいますが、正直なところ、もっと電気工事がやりたいのです。資格を取得することで工事の幅が広がります。自分自身の可能性を限定しないでどんどんと取り組んでいきます。</p>
              </section>
            </div>
            <div class="wrapper mt20 sp-mt0">
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_11.png" alt="職場復帰してから新たに始めたこと"></h3>
                <p>通常業務外の話になりますが、会社の福利厚生の取り組みとしてサークル設立できるようになりました。以前から社内のワーキングマザーの方々のために何か出来ないかと考えていたので、サークル設立をすぐに思い立ちました。ぜひ男性にも参加して頂きたかったので “パパママサークル”という名称で2015年6月に設立し、活動を開始しました。<br>パパママサークルは平日のランチタイムが主な活動時間です。仕事・家事・育児に気兼ねすることなく、所属部署関係なく“交流できる
                  場”として、また、継続して働くうえで“安心できる場所”として機能させたかったためです。<br>会社公認のため心強いですし、会食費はサークル活動費から出るので個人負担はありません。私自身、出産前から色々な方にお世話になりましたが、異なる部署に点在されていました。その経験もあり、これから私が個人の方々を応援していくには、あまりにもあいまいで計画性が無かったので、サークルは最適な形でした。うれしいことに設立初年度から妊婦さんが数名参加して下さり、現在は産休制度取得中です。2016年度はその方々向けに、土日の活動で職場復帰前の相談会やウォーミングアップ会なども行いたいと考えています。通常業務をきちんと行うことはもちろんですが、それを支えるメンタル面のケアも長く勤務していく上で重要だと思います。<br>サークル活動を通して、楽しく業務へのモチベーションアップをはかっていくことが目標です。当サークルの設立に力添えしてくれたサークルメンバーの方々、サークル活動を許可して下さった関係者の方々に感謝しています。</p>
                </section>
                <section class="p-voice-cont sp-mt20">
                  <h3><img src="../images/employee/voice_title_12.png" alt="仕事に対する今後の展望"></h3>
                  <p><img src="../images/employee/voice04_img_02.jpg" alt=""></p>
                  <p>どんなに気をつけていても子供の急病はありますし、それによって予定していた業務ができないこともあります。子育てをしながら仕事をするのは思っていた以上にハードですが、会社はじめ、同僚の理解がありますので安心です。今後は、無理のない範囲で勤務を続けて、あとに続くワーキングマザーの方たちのためにも、長く勤務を続ける上での良いアイディアや意見などを挙げていけたらいいなと思います。</p>
                </section>
              </div>
              <div class="wrapper mt20 sp-mt0 pb70">
                <section class="p-voice-cont sp-mt20">
                  <h3><img src="../images/employee/voice_title_13.png" alt="家事との両立で工夫していること"></h3>
                  <p class="p-mg-txt">お昼休憩中にネットで料理レシピを検索し、当日の夕飯と翌日の朝食メニューを決めています。帰宅後は、就寝時以外はほとんど家事で動きまわっているか、子供の相手をしているので落ち着ける時間はありません。<br>そのため、空き時間が少しでもあれば有効に活用するようになりました。</p>
                </section>
              </div>
              <section>
                <h3 class="p-blue_bg">就活生のみなさんへ</h3>
                <p class="p-mg-txt ml20 sp-center sp-mt20">産休・育休の取得に不安を持たれている方はいると思いますが、当社では取得者も増えており、問題なく取得できますので安心してください。<br>また、誰でも初めての出産は不安を抱えるものです。<br>解決を求める類の話でなくても、ただ話ができる人がいる環境も大切だと思いますし、そういった雰囲気があるのが当社の魅力だと思います。</p>
              </section>
            </div>
          </section>
          <section class="p-inquiry mt50">
            <div class="l-wrap-02">
              <div class="p-inquiry-box">
                <div class="p-box-left">
                  <div class="sp_none">
                    <a href="../seminar/"><img class="imghover" src="../images/employee/seminar_bnr.png" alt="SEMINAR" style="opacity: 1;"></a>
                  </div>
                  <div class="pc_none">
                    <a href="../seminar/">
                      <div class="p-text-left">
                        <img src="../images/employee/sp_seminar_bnr_01.png" alt="SEMINAR">
                      </div>
                      <div class="p-text-right">
                        <img src="../images/employee/sp_seminar_bnr_02.png" alt="SEMINAR">
                      </div>
                    </a>
                  </div>
                </div>
                <div class="p-box-right">
                  <a href="../internship/"><img class="imghover" src="../images/employee/intern_bnr.png" alt="INTERNSHIP" style="opacity: 1;"></a>
                </div>
              </div>
            </div>
          </section>
          <!-- l-content --></div>


          <!-- l-pageBody --></div>

          <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
          <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
        </body>
        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
        </html>



