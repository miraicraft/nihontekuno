<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

  <title>森 啓騎 | 社員紹介 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
  <meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
  <meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報,インタビュー,営業">
  <link rel="canonical" href="#">

  <!-- ページ共通のCSSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
  <!-- ページ共通のCSSファイル終了-->

  <!-- ページ共通のJSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
  <!-- ページ共通のJSファイル終了-->

  <!-- ページ固有のCSSファイル開始-->
  <link rel="stylesheet" href="../css/employee.css">
  <!-- ページ固有のCSSファイル終了-->

  <!-- ページ固有のJSファイル開始-->
  <!-- ページ固有のJSファイル終了-->

  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

  <div class="l-pageBody">

    <nav class="l-topicPath">
      <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="./">
            <span itemprop="name">社員紹介</span></a>
            <meta itemprop="position" content="1" />
          </li>
          <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            ＞<a itemprop="item" href="voice03.html">
            <span itemprop="name">森　啓騎</span></a>
            <meta itemprop="position" content="2" />
          </li>
        </ol>
      </nav>

      <div class="l-content">
        <section class="p-voice03">
          <div class="p-mv">
            <h2><img src="../images/employee/voice03_mv_title.png" alt="MORI HIROKI"></h2>
            <p class="p-sub-title mt35 sp-mt20"><img src="../images/employee/voice03_mv_txt.png" alt="電気や機械を通してお客様と接点のある会社に入社したかった"></p>
            <div class="p-mv-box">
              <p>森　啓騎<br>技術本部　技術サービス部　第一課第三係　さいたま班<br>玉川大学工学部機械情報システム学科卒　2014年新卒入社</p>
            </div>
          </div>

          <div class="p-voice-wrap">
            <div class="wrapper mt50">
              <section class="p-voice-cont">
                <h3><img src="../images/employee/voice_title_01.png" alt="入社のきっかけ"></h3>
                <p>高専にて電気を学び、自分の可能性を限定したくないと考え、機械を勉強するために玉川大学に編入しました。学生の頃にアルバイトをしていたガソリンスタンドに日本テクノの社用車がよく来店いただいておりました。電気関係の会社だと思い、調べてみたのがきっかけです。<br>アルバイトでの接客の楽しさもあり、電気や機械を通してお客様との接点のある会社に入社したいと考えていた私にとっては、ぴったりの会社だったので、入社を決めました。</p>
              </section>
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_02.png" alt="入社してみて感じたこと"></h3>
                <p>自社商品の設置が主な仕事です。正しく設置されないと会社として正しいサービスが提供できないので、安全に丁寧に設置しています。横浜市を中心にESシステム、ERIA、SMART CLOCKだけではなく、エコテナントやLVMの設置もしています。特にエコテナントの設置は土曜、日曜が多いですが、代休はしっかりと取れます。休みの日は、第1種電気工事士の資格取得に向けて勉強しています。<br>多くの経験を積んでいますが、正直なところ、もっと電気工事がやりたいのです。資格を取得することで工事の幅が広がります。自分自身の可能性を限定しないでどんどんと取り組んでいきます。</p>
              </section>
            </div>
            <section class="pb70">
              <h3 class="p-bd mt40"><img src="../images/employee/voice_title_08.png" alt="新卒社員の一日　ONEDAY"></h3>
              <ul class="p-timeline">
                <li>
                  <div class="p-timeline-content">
                    <h4>08:30　出社</h4>
                    <p class="p-txt">着替えをして、本日のスケジュール確認</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>09:00　朝礼</h4>
                        <p class="p-txt">保安部と技術サービス部の合同で実施し、本日のスケジュール確認<br>朝礼後は本日の現場調査とESシステムの再設置の作業報告書の準備さらに、事前に出来る各種機器の設定を行い現場での作業効率化を図る</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice03_img_01.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>09:50　営業所出発</h4>
                        <p class="p-txt">作業は2名が原則である。<br>先輩社員にいろいろと聞きながら現地へ。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice03_img_02.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>10:30　到着</h4>
                        <p class="p-txt">A社到着　お客様と設備の場所や注意事項の確認を行い、現場調査へ<br>既にESシステムは設置されているが、追加で保安点検を日本テクノに変更した場合の見積り依頼と、新館の設備確認のための現場調査を実施。既設の容量確認と漏電値測定を行い、ESシステムとの表示を確認。その後、新設物件である新館の設備容量、漏電値測定および非常用発電機の容量を確認。これをもとに営業スタッフの提案が行われるため、間違ってはいけない。丁寧に確認していく。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice03_img_03.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>11:25　現場調査終了</h4>
                    <p class="p-txt">お客様に確認が終わったことをお伝えし退社</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>12:15　次のお客様の近くまで移動し、<br>車中にてお弁当でランチ</h4>
                        <p class="p-txt">好きなものばかり食べてしまうので、栄養の偏りがないように。体調管理も仕事です。 </p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice03_img_04.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>13:00　B社到着　お客様にキュービクルの<br>場所を確認して再設置作業に</h4>
                        <p class="p-txt">前日、先方より「じゃ、お願いするよ」と内諾をもらっていた会社。契約書への署名、捺印が完了したとの一報を受けて再度訪問した。すべての書類に不備がないことを確認し、無事契約に至った。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice03_img_05.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>14:30</h4>
                        <p class="p-txt">接続が完了し、監視センターへのテスト通報を行い、<br>確認の電話を入れる。さらに電力会社にパルス接続の依頼を入れて、接続日程の調整を行う。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice03_img_06.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>14:45　作業完了</h4>
                    <p class="p-txt">お客様に終了報告をし、ERIAに電波が届いていることを確認して退社</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>16:00　営業所到着</h4>
                        <p class="p-txt">本日の作業で使った備品を補充しながら、営業所内のすべての在庫管理も行う。細かな部品も見落として足りないということがあってはいけない。先々の工事予定や営業の活動状況も意識しながら補充をしていく。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice03_img_07.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>17:00　作業完了報告書作成</h4>
                    <p class="p-txt">どんな設備があり、どこに設置したのか、しっかりと共有できるように丁寧に。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>18:00　退社　お疲れ様です。</h4>
                    <p class="p-txt">先週の金曜・土曜がエコテナント設置、日曜がLVM設置。取材日前日と翌日は代休としているが、やっぱり疲れる。しっかりと休み、次の設置工事に備える。</p>
                  </div>
                </li>
              </ul>
            </section>
            <div class="wrapper pb70">
              <section class="p-voice-cont">
                <h3><img src="../images/employee/voice_title_03.png" alt="仕事の息抜き法"></h3>
                <p><img src="../images/employee/voice03_img_08.jpg" alt=""></p>
                <p>移動中、休憩中の上司や同僚との雑談。</p>
              </section>
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_04.png" alt="休日の過ごし方"></h3>
                <p><img src="../images/employee/voice03_img_09.jpg" alt=""></p>
                <p>学生時代からの仲間と車で山やサーキットに行きます。<br>業務でも車を運転するので練習代わりにもなりますね。</p>
              </section>
            </div>
            <section>
            <h3 class="p-blue_bg">就活生への応援メッセージ</h3>
              <div class="wrapper">
                <p class="p-mg-img mr40 sp-center sp-mt20"><img src="../images/employee/voice03_img_10.jpg" alt=""></p>
                <p class="p-mg-txt">ここだ！と思えるポイントを見つければ自然と気持ちがのってきます。<br>情報を見極めて最後は自分の感覚を信じること！</p>
              </div>
            </section>
          </div>
        </section>
        <section class="p-inquiry mt50">
          <div class="l-wrap-02">
            <div class="p-inquiry-box">
              <div class="p-box-left">
                <div class="sp_none">
                  <a href="../seminar/"><img class="imghover" src="../images/employee/seminar_bnr.png" alt="SEMINAR" style="opacity: 1;"></a>
                </div>
                <div class="pc_none">
                  <a href="../seminar/">
                    <div class="p-text-left">
                      <img src="../images/employee/sp_seminar_bnr_01.png" alt="SEMINAR">
                    </div>
                    <div class="p-text-right">
                      <img src="../images/employee/sp_seminar_bnr_02.png" alt="SEMINAR">
                    </div>
                  </a>
                </div>
              </div>
              <div class="p-box-right">
                <a href="../internship/"><img class="imghover" src="../images/employee/intern_bnr.png" alt="INTERNSHIP" style="opacity: 1;"></a>
              </div>
            </div>
          </div>
        </section>
        <!-- l-content --></div>


        <!-- l-pageBody --></div>

        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
      </body>
      <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
      </html>



