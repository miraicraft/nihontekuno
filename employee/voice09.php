<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

  <title>福田 貴志 | 社員紹介 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
  <meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
  <meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報,インタビュー,営業">
  <link rel="canonical" href="#">

  <!-- ページ共通のCSSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
  <!-- ページ共通のCSSファイル終了-->

  <!-- ページ共通のJSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
  <!-- ページ共通のJSファイル終了-->

  <!-- ページ固有のCSSファイル開始-->
  <link rel="stylesheet" href="../css/employee.css">
  <!-- ページ固有のCSSファイル終了-->

  <!-- ページ固有のJSファイル開始-->
  <!-- ページ固有のJSファイル終了-->

  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

  <div class="l-pageBody">

    <nav class="l-topicPath">
      <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="./">
            <span itemprop="name">社員紹介</span></a>
            <meta itemprop="position" content="1" />
          </li>
          <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            ＞<a itemprop="item" href="voice09.html">
            <span itemprop="name">福田　貴志</span></a>
            <meta itemprop="position" content="2" />
          </li>
        </ol>
      </nav>

      <div class="l-content">
        <section class="p-voice09">
          <div class="p-mv">
            <h2><img src="../images/employee/voice09_mv_title.png" alt="IFUKUDA TAKASHI"></h2>
            <p class="p-sub-title mt35 sp-mt20"><img src="../images/employee/voice09_mv_txt.png" alt="「格好の悪いところは見せられない」の思いで、何事にも一生懸命に取り組んだ"></p>
            <div class="p-mv-box">
              <p>福田　貴志<br>東日本営業部　圏央支店　立川営業所（取材当時）<br>立川営業所　所長（現在）神奈川大学経済学部卒　2012年新卒入社</p>
            </div>
          </div>

          <div class="p-voice-wrap">
            <div class="wrapper mt50">
              <section class="p-voice-cont">
                <h3><img src="../images/employee/voice_title_01.png" alt="入社のきっかけ"></h3>
                <p>大学時代に地元である淵野辺の居酒屋でアルバイトをしていました。その居酒屋によく来店されるお客様の中に、ひときわ興味を抱く方々がいらっしゃいました。当時の神奈川支店相模原営業所の綿引支店長と田中所長でした。お二人と何名かでのご来店が多く、いつも高めのコースを頼まれていたので、「景気の良い会社だな」との印象と、お会計の後、お店を出られたところで、「やるぞ！やるぞ！やるぞ！」のかけ声に「元気のある会社だな」と思っていました。就職活動では、日本テクノと大手鉄道会社を受けました。「自分自身にとって身近な存在であり、元気があり、なにより楽しそう」との思いで、入社。</p>
              </section>
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_02.png" alt="入社してみて感じたこと"></h3>
                <p>ご来店いただいていた田中所長のもとに配属されたのも良かったです。知っている方が上司でもあったため、「格好の悪いところは見せられない」との思いが強く、何事にも一生懸命に取り組みました。一丸となって戦っている雰囲気を垣間見ていたこともあり、研修や営業所の雰囲気も違和感はなく、楽しかったです。</p>
              </section>
            </div>
            <section class="pb70">
              <h3 class="p-bd mt40"><img src="../images/employee/voice_title_08.png" alt="新卒社員の一日　ONEDAY"></h3>
              <ul class="p-timeline">
                <li>
                  <div class="p-timeline-content">
                    <h4>08:00　出社</h4>
                    <p class="fll sp-fln txt">本日の訪問先への資料の確認やメールのチェックなど。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>08:45　朝礼</h4>
                        <p class="fll sp-fln txt">声出し、経営理念、企業理念などの唱和後、各人からの行動予定、連絡事項などがあり、最後は副所長としてメンバー全員への激励。所員の全員が年上だが、営業所のNo.2として役割も多い。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice09_img_01.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>09:00　出発</h4>
                        <p class="p-txt">BEMS導入遊戯店へGIFT訪問（アフターフォローとしての定期訪問） 2件の店舗があり、本日は導入1年を超えた１店舗へのアポイント。少し早めに到着をして、導入2ヶ月のもう1店舗へ飛び込みで訪問。<br>「年末のご挨拶にお伺いしました」と開店直前の訪問にも店長さんが出て来られました。「何かお困りのことはありませんか」に対して「お客様から少し寒いとの声が出始めた」と省エネ活動の推進はあるものの、お客様に迷惑をかけてはいけない。もう１店舗も導入当初に起きた問題なので、これから訪問するもう１店舗とも相談しながら、活動の工夫を共有していくことを話し、カレンダーをお渡しし、退出する。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice09_img_02.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>10:30　本日の最初のアポイントA社</h4>
                        <p class="p-txt">お約束をしていた専務は突然の来客でどうしても席が外せないとのことでしたが、店長が開店直後にも関わらずお話を聞いていただきました。この1年間の成果報告と48コマ設定の冬バージョンへの変更の相談が目的。年間で50万円程度の削減がありました。すでにGIFTを7回実施しており、その評価も十分であったため、非常にスムーズに話は進みました。<br>このお客様の導入してからの課題は、いつも店長が出社するタイミングに警報が鳴ることであった。最初の目標設定値はひとつにしていたため、最大ピークのみで対応となり、そのタイミングが店長出社に偏ってしまっていた。これでは従業員の意識改革にはつながらないし、「私だけでは意味がない」との対応策として、早々に48コマ設定（時間帯毎の目標設定）で省エネ活動を実践している。<br>お客様の声として「暑い」「寒い」が増えている。すべてに対応はしているが、時間帯によっては多少の我慢をお願いしている。この点をなんとか工夫したいと店長からの要望があり、もう1件でも同様の話があったことをお伝えし、再度、対策を練ることをお約束し、本日は失礼した。次のアポイントまでに昼食と移動。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice09_img_03.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>13:30　老人ホームB社訪問</h4>
                        <p class="p-txt">こちらもBEMS導入企業であり、GIFTは7回実施済みで効果も出ている。当初はなかなかコミュニケーションが取れずに、お客様からお叱りを受けていたが、ひとつひとつ丁寧に対応することで信頼を得ることができた。48コマ設定値を冬場になったので、変更の相談が目的であった。<br>最近のデータを元に警報回数を確認すると、1日に7～8回であった。事務長は警報が鳴ることによって、従業員が行動を起こすことが重要であり、意識が変わることが一番大切である、とまさしく日本テクノが目指すべき姿を明言された。入居者へのサービスを脱しない的確な対策を打つことが現場で出来ないようでは困ると従業員への信頼と更なる要望をお話しいただいた。「もっと多くの従業員にも知らせたい」との要望に対して、「電話通報への登録数を増やしましょう」と提案し、早速2ヵ所の電話番号をご提示いただき、早々に登録が完了した。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice09_img_04.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>15:00　刃物工具部品製造C社訪問</h4>
                        <p class="p-txt">省エネ活動改善カルテを前回摺合せして、その修正点の確認および今後の勉強会等々の打ち合わせ目的での訪問。<br>「事務所はわかるが、工場では最終退出者が自分なのかが判明しづらいことがあり、Do-Checkをどのように機能させるべきかを社員と話し、解決策が見つかったので、1月中旬よりスタート予定です」とあり、さらにその方法を確認（稼働状況を確認するまでは内緒です）。いずれにしても、夏にピークを迎えるので、それまでに勉強会や見直しを進めていくことを確認する。その後に、山梨工場の従業員の意識がもう少し足りないことを指摘され、本社と同様の勉強会の開催などの対策を打ち合わせ。その後に、社長から最近の悩み事の相談を受ける。「最近、電話での反応が少し変に感じた原因がわかりました」と福田副所長。少し経営者の視点でお客様と接することが出来始めた。<br>最後に電気炉の必要温度とスイッチを入れるタイミングを夏前までに整理していくことをお客様の宿題として約束をして退出する。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice09_img_05.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>16:30　営業所に向かう</h4>
                    <p class="p-txt">途中で、所員からのクロージングに関しての相談があり、営業車を止めて、対策を指示する。日々、所員からの相談や報告を受けながらの自分自身の営業活動は負担ではあるが、やりがいを感じている。ただ、思うように進まないことが最近の悩みでもある。しかし、刃物部品製造会社の社長の悩みにすれば、小さなことと、前を向いた。 </p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>17:30　営業所に戻る</h4>
                    <p class="p-txt">お客様からの問い合わせへの対応、アポイントの電話などを進めながら、本日の成果報告や相談などを行い、終礼を迎える。 </p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>18:00　終礼</h4>
                        <p class="p-txt">営業所として今月の数字の確認や商談の進捗確認。すべての報告に対して、「質問のある人？」と所長からの言葉に必ず誰かが質問をぶつけている。この情報共有が有効であり、わからないことをその場で解決している。最後には「情報共有」コーナーもあり、本日、自分にとって役に立ったことや気付きを共有する。2014年11月度全国トップ営業所には何かがある。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice09_img_06.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>19:00　退社</h4>
                    <p class="p-txt">「本日もお疲れ様でした」</p>
                  </div>
                </li>
              </ul>
            </section>
            <div class="wrapper pb70">
              <section class="p-voice-cont">
                <h3><img src="../images/employee/voice_title_03.png" alt="仕事の息抜き法"></h3>
                <p><img src="../images/employee/voice09_img_07.jpg" alt=""></p>
                <p>机の掃除をします。業務効率アップと、気持ちが落ち着くので一石二鳥です。</p>
              </section>
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_04.png" alt="休日の過ごし方"></h3>
                <p><img src="../images/employee/voice09_img_08.jpg" alt=""></p>

                <p>営業所員、サークルメンバー、地元の友人らとよく飲みに行きます。<br>日本テクノではアウトドアサークルに入っています。</p>
              </section>
            </div>
            <section>
              <h3 class="p-blue_bg">就活生への応援メッセージ</h3>
              <div class="wrapper">
                <p class="p-mg-img mr40 sp-center sp-mt20"><img src="../images/employee/voice09_img_09.jpg" alt=""></p>
                <p class="p-mg-txt">最後に迷ったら人で選べば間違いないと思います。出来るだけ多くの<br>人に会って自分に合う会社を見つけてください。</p>
              </div>
            </section>
          </div>
        </section>
        <section class="p-inquiry mt50">
          <div class="l-wrap-02">
            <div class="p-inquiry-box">
              <div class="p-box-left">
                <div class="sp_none">
                  <a href="../seminar/"><img class="imghover" src="../images/employee/seminar_bnr.png" alt="SEMINAR" style="opacity: 1;"></a>
                </div>
                <div class="pc_none">
                  <a href="../seminar/">
                    <div class="p-text-left">
                      <img src="../images/employee/sp_seminar_bnr_01.png" alt="SEMINAR">
                    </div>
                    <div class="p-text-right">
                      <img src="../images/employee/sp_seminar_bnr_02.png" alt="SEMINAR">
                    </div>
                  </a>
                </div>
              </div>
              <div class="p-box-right">
                <a href="../internship/"><img class="imghover" src="../images/employee/intern_bnr.png" alt="INTERNSHIP" style="opacity: 1;"></a>
              </div>
            </div>
          </div>
        </section>
        <!-- l-content --></div>


        <!-- l-pageBody --></div>

        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
      </body>
      <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
      </html>



