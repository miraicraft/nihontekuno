<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

  <title>倉元 梨沙 | 社員紹介 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
  <meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
  <meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報,インタビュー,営業">
  <link rel="canonical" href="#">

  <!-- ページ共通のCSSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
  <!-- ページ共通のCSSファイル終了-->

  <!-- ページ共通のJSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
  <!-- ページ共通のJSファイル終了-->

  <!-- ページ固有のCSSファイル開始-->
  <link rel="stylesheet" href="../css/employee.css">
  <!-- ページ固有のCSSファイル終了-->

  <!-- ページ固有のJSファイル開始-->
  <!-- ページ固有のJSファイル終了-->

  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

  <div class="l-pageBody">

    <nav class="l-topicPath">
      <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="./">
            <span itemprop="name">社員紹介</span></a>
            <meta itemprop="position" content="1" />
          </li>
          <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            ＞<a itemprop="item" href="voice05.html">
            <span itemprop="name">倉元　梨沙</span></a>
            <meta itemprop="position" content="2" />
          </li>
        </ol>
      </nav>

      <div class="l-content">
        <section class="p-voice05">
          <div class="p-mv">
            <h2><img src="../images/employee/voice05_mv_title.png" alt="KURAMOTO RISA"></h2>
            <p class="p-sub-title mt35 sp-mt20"><img src="../images/employee/voice05_mv_txt.png" alt="アフターフォローによる信頼から紹介が生まれていく営業がやりたかった"></p>
            <div class="p-mv-box">
              <p>倉元　梨沙<br>営業部　中部支店　名古屋第一営業所<br>椙山女学園大学国際コミュニケーション学部卒　2013年新卒入社</p>
            </div>
          </div>

          <div class="p-voice-wrap">
            <div class="wrapper mt50">
              <section class="p-voice-cont">
                <h3><img src="../images/employee/voice_title_01.png" alt="入社のきっかけ"></h3>
                <p>大学で国内外の文学や映像作品、音楽などの表現文化を学び、世界の中の日本という視点を身に付けていきました。多くの友人が表現文化の道に進んでいくのですが、私は、自分自身を表現していきたいと考え、営業職を目指していくようになりました。営業職なら安定性のある「インフラ」ビジネスと考え、電気やガスなどを中心に会社研究をしていきました。アフターフォローによる信頼から紹介が生まれていく営業スタイルに「これこそが私がやりたい営業だ」と、2次面接以降は日本テクノを第一希望として、無事に入社できました。</p>
              </section>
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_02.png" alt="入社してみて感じたこと"></h3>
                <p>内定者研修では「正直、間違えたかな」と思いましたが、入社する前から判断してはいけないと、その気持ちを隠していました。ところが、実際に入社すると、充実した研修があり、どの先輩に質問をしても必ず回答していただきました。わからないことがどんどんとクリアになっていく毎日でした。日々の業務に追われてはいましたが、自らが仕事をしているという実感と自分の成長を自身で感じられる成長感がありました。入社4年目になり、後輩も増えました。目指される先輩になるように、さらに気を引き締めて努力していきます。</p>
              </section>
            </div>
            <section class="pb70">
              <h3 class="p-bd mt40"><img src="../images/employee/voice_title_08.png" alt="新卒社員の一日　ONEDAY"></h3>
              <ul class="p-timeline">
                <li>
                  <div class="p-timeline-content">
                    <h4>08:30　出社</h4>
                    <p class="p-txt">本日の訪問予定の準備と最終確認</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>08:45　ロールプレイング</h4>
                    <p class="p-txt">今日の訪問予定をイメージしたロールプレイングを実施。<br>営業所内でペアになってアドバイスをしたり、されたり。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>09:00　全体朝礼</h4>
                        <p class="p-txt">曜日によって変わるが、本日はマネージャーの心得を全員で唱和。その場で司会者が指名した方の1分間スピーチ。<br>全体への連絡事項が行われる。今回は日本テクノが取り組む環境プログラムについての報告が行われた。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice05_img_01.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>09:15　営業所朝礼</h4>
                    <p class="p-txt">本日の行動予定と成果目標を共有。倉元班長による新聞記事解説。刻々と変化する電力業界の動向など学ぶことは多い。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>09:30</h4>
                    <p class="p-txt">準備物の最終確認や後輩からの相談など朝はいろいろと忙しい </p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>10:20　営業所出発</h4>
                        <p class="p-txt">営業所から5分弱のところにある駐車場。今日も安全運転で出発。 </p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice05_img_02.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>11:00　A社にて商談</h4>
                        <p class="p-txt">4月から担当になったユーザーに更新契約に向けての打ち合わせ。お客様の3件の高圧物件の満足度は高く良い方向で話は進む。電気設備の保安点検からデマンド対策によって効果が出ており、電気料金削減が実現している。<br>さらに、電力小売を行うようになり、日本テクノのサービスに非常に満足をいただいている状態であった。現在の契約期間満期の3ヶ月前には更新契約手続きの約束をいただき、事前に見積書の提出を求められる。そのお客様には他にも低圧物件があり、前任との行き違いでトラブルになっていた。その解決に向けて支店長が対応。これからは自分のお客様になるので、このトラブルにも真摯に前向きに取り組んでいくことを誓った。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice05_img_03.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>11:45　A社退社</h4>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>12:30　ランチ</h4>
                    <p class="p-txt">4月から担当することになった新しいエリアまで移動しランチ</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>13:30　B社訪問予定が</h4>
                        <p class="p-txt">B社訪問予定が表記されていた住所に会社が無い！お客様に電話するも急な来客中で場所を教えている時間は無いので、改めて連絡をくださいとのことになる。<br>次のアポイントが15:30までの時間は無駄に出来ない。その場で訪問しておくべきお客様に何件が電話をして、1件のアポイントを取る。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice05_img_04.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>14:00　C社訪問</h4>
                        <p class="p-txt">2週間前に導入後の勉強会を実施した会社。その後の省エネ活動の進捗確認のために訪問。勉強会以降、従業員が自主的に省エネ活動を行っており、社長は大喜びであった。むし暑くなっていた工場は空調ではなく、扇風機が回っていた。社長は空調を点ければ良いと言っても、従業員が「まだ、扇風機で充分です」と空調を入れない。「日本テクノの倉元さんが電気料金の仕組みや省エネの仕方を教えてくれたおかげだよ」と外出予定を遅らせてまで訪問をOKしてくれていた。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice05_img_05.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>14:00　C社訪問</h4>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>15:30　D社訪問</h4>
                        <p class="p-txt">何度か日本テクノから営業は受けているが、導入に至っていないお客様。改めて現在の日本テクノのサービスを紹介する。空調利用方法、照明の間引きなど、担当役員は、しっかりと対策を打っていたが、昨年の8月にデマンドが出ていた。この原因が分からないことが会話の中から出てきた。WEB上で電力使用状況が確認できる「デマンド閲覧サービス」に興味を持ってもらえた。電気使用状況2年分のデータと高圧受変電設備点検簿のコピーをいただき、翌週に提案書の提出のアポイントをお約束。さらに各フロアの設備状況をご案内いただいた。新規契約に向けて、一歩前進。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice05_img_06.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>17:00　D社退社</h4>
                    <p class="p-txt">本日の結果をメールにて所長へ報告</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>17:50　帰社</h4>
                    <p class="p-txt">支店長から声をかけられ、報連相。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>18:15　終礼</h4>
                        <p class="p-txt">本日の行動結果の報告と共有。月初に立てた目標の達成度を全員でチェック。気になる点をお互いに指摘し合う。リーダーシップを持って全体へ発信し、「省エネ・運用改善カルテ」の進捗管理を率先して行っている。<br>アフターフォローの充実が新規顧客開拓につながっている。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice05_img_07.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>18:40</h4>
                    <p class="p-txt">本日の行動結果の入力と明日の準備に取り掛かる。<br>集中して効率よく行う。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>19:20　退社</h4>
                    <p class="p-txt">お疲れ様でした。帰って缶ビールで乾杯！</p>
                  </div>
                </li>
              </ul>
            </section>
            <div class="wrapper pb70">
              <section class="p-voice-cont">
                <h3><img src="../images/employee/voice_title_03.png" alt="仕事の息抜き法"></h3>
                <p><img src="../images/employee/voice05_img_08.jpg" alt=""></p>
                <p>好きな歌を聞きながら、ストレッチをして体をほぐします。</p>
              </section>
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_04.png" alt="休日の過ごし方"></h3>
                <p><img src="../images/employee/voice05_img_09.jpg" alt=""></p>
                <p>友人や同期と旅行に行ったり、会社のサークル活動で体を動かすなど、<br>皆と交流を深めながら楽しんでいます。</p>
              </section>
            </div>
            <section>
              <h3 class="p-blue_bg">就活生への応援メッセージ</h3>
              <div class="wrapper">
                <p class="p-mg-img mr40 sp-center sp-mt20"><img src="../images/employee/voice05_img_10.jpg" alt=""></p>
                <p class="p-mg-txt">良いと思ったらその直感を信じる事も大切です。まずは挑戦する気持ちで<br>たくさんの方々と話しましょう。前向きに頑張って下さい！</p>
              </div>
            </section>
          </div>
        </section>
        <section class="p-inquiry mt50">
          <div class="l-wrap-02">
            <div class="p-inquiry-box">
              <div class="p-box-left">
                <div class="sp_none">
                  <a href="../seminar/"><img class="imghover" src="../images/employee/seminar_bnr.png" alt="SEMINAR" style="opacity: 1;"></a>
                </div>
                <div class="pc_none">
                  <a href="../seminar/">
                    <div class="p-text-left">
                      <img src="../images/employee/sp_seminar_bnr_01.png" alt="SEMINAR">
                    </div>
                    <div class="p-text-right">
                      <img src="../images/employee/sp_seminar_bnr_02.png" alt="SEMINAR">
                    </div>
                  </a>
                </div>
              </div>
              <div class="p-box-right">
                <a href="../internship/"><img class="imghover" src="../images/employee/intern_bnr.png" alt="INTERNSHIP" style="opacity: 1;"></a>
              </div>
            </div>
          </div>
        </section>
        <!-- l-content --></div>


        <!-- l-pageBody --></div>

        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
      </body>
      <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
      </html>



