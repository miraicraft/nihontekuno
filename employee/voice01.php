<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
	<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
	<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

	<title>佐藤 和広 | 社員紹介 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
	<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
	<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報,インタビュー,営業">
	<link rel="canonical" href="#">

	<!-- ページ共通のCSSファイル開始-->
	<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
	<!-- ページ共通のCSSファイル終了-->

	<!-- ページ共通のJSファイル開始-->
	<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
	<!-- ページ共通のJSファイル終了-->

	<!-- ページ固有のCSSファイル開始-->
	<link rel="stylesheet" href="../css/employee.css">
	<!-- ページ固有のCSSファイル終了-->

	<!-- ページ固有のJSファイル開始-->
	<!-- ページ固有のJSファイル終了-->

	<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
	<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
	<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

	<div class="l-pageBody">

		<nav class="l-topicPath">
			<ol itemscope itemtype="http://schema.org/BreadcrumbList">
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a itemprop="item" href="./">
						<span itemprop="name">社員紹介</span></a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						＞<a itemprop="item" href="voice01.html">
						<span itemprop="name">佐藤 和広</span></a>
						<meta itemprop="position" content="2" />
					</li>
				</ol>
			</nav>

			<div class="l-content">
        <section class="p-voice01">
          <div class="p-mv">
            <h2><img src="../images/employee/voice01_mv_title.png" alt="SATOU KAZUHIRO"></h2>
            <p class="p-sub-title mt35 sp-mt20"><img src="../images/employee/voice01_mv_txt.png" alt="資格取得に必要な実務経験を積めることが決め手だった"></p>
            <div class="p-mv-box">
              <p>佐藤 和広<br>保安部　東日本担当　第三課　第一係<br>千葉工業大学 電気電子情報工学科卒 2013年新卒入社</p>
            </div>
          </div>

          <div class="p-voice-wrap">
            <div class="wrapper mt50">
             <section class="p-voice-cont">
               <h3><img src="../images/employee/voice_title_01.png" alt="入社のきっかけ"></h3>
               <p>情報工学はパソコン、電子は小さな回路のイメージがあり、なんとなく自分の進みたい方向とは違うという漠然とした思いがあり、電気の道へと進みました。また、兄が土木施工の仕事をしていて、自分は施工向きではないと言われたことで、設備点検へと自分の道を決めていきました。就活にあたり、電気の設備点検で調べると、日本テクノが出てきましたが、セミナーに参加しても営業向けの内容だったため、本当に募集をしているのか心配でした。しかし人事に確認したところ募集をしているとのことだったので、応募しました。ビル管理会社と悩みましたが、最終的には資格取得ができることが決め手でした。さらに定年後も日本テクノ協力会・日電協に入会することで年金の不安もなくなるという点にも魅力を感じました。</p>
             </section>
             <section class="p-voice-cont sp-mt20">
               <h3><img src="../images/employee/voice_title_02.png" alt="入社してみて感じたこと"></h3>
               <p>保安部では初めての新卒採用で私を含む2名が入社しました。最初の2週間は営業スタッフの同期と同じ研修を受け、その後3ヶ月間、相模原の研修施設でみっちり研修を受けました。検査器の操作方法なども含めて安全に確実に実施できるように反復しました。このときの研修経験が現在も活きています。特に年次点検などの停電を伴う作業は、復電までの時間を意識して、安全第一に確実に実施することができています。まだまだ経験と勉強は必要ですが…。</p>
             </section>
           </div>
           <section class="pb70">
             <h3 class="p-bd mt40"><img src="../images/employee/voice_title_06.png" alt="年次点検応援の日　ONEDAY"></h3>
             <ul class="p-timeline">
               <li>
                 <div class="p-timeline-content">
                   <h4>07:00　東京サービスセンター集合</h4>
                   <p class="p-txt">必要な用具や機器の積み込みをする。今日の物件は設備が<br>大きいため、主任技術者を含めて4名で実施。</p>
                 </div>
               </li>
               <li>
                 <div class="p-timeline-content">
                   <div class="wrapper">
                     <div class="fll sp-fln">
                      <h4>07:30　出発</h4>
                      <p class="p-txt">車内では今日の作業確認や雑談でコミュニケーションを取る。</p>
                    </div>
                    <p class="flr sp-fln"><img src="../images/employee/voice01_img_01.jpg" alt=""></p>
                  </div>
                </div>
              </li>
              <li>
               <div class="p-timeline-content">
                 <div class="wrapper">
                   <div class="fll sp-fln">
                    <h4>08:00　年次点検の現場に到着</h4>
                    <p class="p-txt">お客さまに確認を取りながら機材を搬入。9階屋上までエレベーターで機材を運び、そこから階段を上がったところにあるキュービクルまで分担して歩いて登っていく。<br>自ら重たい機材の運び役を買って出る。</p>
                  </div>
                  <p class="flr sp-fln"><img src="../images/employee/voice01_img_02.jpg" alt=""></p>
                </div>
              </div>
            </li>
            <li>
             <div class="p-timeline-content">
               <h4>08:40　準備完了</h4>
               <p class="p-txt">点検準備と設備の確認。お客様からの電話連絡に合わせて停電していくが、それまでの時間にも準備と設備の確認を進めていく。作業は2時間を予定。停電をさせての作業となるため、復電は約束の時間に間に合わせられるように役割や担当業務、手順も確認をする。</p>
             </div>
           </li>
           <li>
             <div class="p-timeline-content">
               <h4>09:15　年次点検開始</h4>
               <p class="p-txt">キュービクル内を点検。今回の物件は発電機も付いているので、まずはその発電機の点検からスタート。先輩たちと協力しながら確実に安全に点検を進めていく。キュービクル内の点検から、次にキャビネットの点検へと移る。停電しているので、当然エレベーターも止まっている。非常用階段を下りて1階まで。肉体労働と感じる瞬間だ。</p>
             </div>
           </li>
           <li>
             <div class="p-timeline-content">
               <div class="wrapper">
                 <div class="fll sp-fln">
                  <h4>10:00　キャビネットの点検開始</h4>
                  <p class="p-txt">1階駐車場入口にあるキャビネットを点検。UGS試験などを実施して、キュービクルまで戻るが、上りは厳しい。冬晴れではあるが、冷えているため汗は出ない。さらに屋上のキュービクルでの点検は続く。開始が少し遅れたので、復電の時間が迫ってきた。</p>
                </div>
                <p class="flr sp-fln"><img src="../images/employee/voice01_img_03.jpg" alt=""></p>
              </div>
            </div>
          </li>
          <li>
           <div class="p-timeline-content">
             <h4>10:50　キャビネットのUGS投入</h4>
             <p class="p-txt">キュービクルの点検が終了すると、急いで1階キャビネットまで駆け下り、UGSを投入する。移動は迅速に、点検は確実に実行する。投入後、急いでキュービクルのある屋上まで戻る。階段を駆け上がるスピードが速くなる。</p>
           </div>
         </li>
         <li>
           <div class="p-timeline-content">
             <h4>10:55　順次復電開始</h4>
           </div>
         </li>
         <li>
           <div class="p-timeline-content">
             <h4>11:00　年次点検終了</h4>
             <p class="p-txt">お疲れさまでした。無事に点検が終了。復電後の問題もなく、すべての確認を済ませて帰社となった。</p>
           </div>
         </li>
         <li>
           <div class="p-timeline-content">
             <h4>VOICE</h4>
             <p class="p-txt">多くの年次点検の応援に行っていますので、様々な設備や手順を学ぶことができます。ほとんどが初めての物件なので、毎回、何かしらの勉強になっています。わからないことは現地での確認と、帰ってからの再確認で確実に自分自身の知識と経験にしています。</p>
           </div>
         </li>
       </ul>
     </section>
     <section>
       <h3 class="p-bd"><img src="../images/employee/voice_title_07.png" alt="社内業務の日　ONEDAY"></h3>
       <ul class="p-timeline">
         <li>
           <div class="p-timeline-content">
             <h4>08:50　出社</h4>
             <p class="p-txt">本日の予定を確認し、準備を進める。</p>
           </div>
         </li>
         <li>
           <div class="p-timeline-content">
             <div class="wrapper">
               <div class="fll sp-fln">
                <h4>09:00　清掃</h4>
                <p class="p-txt">週に一度は全員で事務所の清掃からスタート。この日は技術者セミナーが開催されるため、その会場準備も先輩たちと一
                  緒に行う。</p>
                </div>
                <p class="flr sp-fln"><img src="../images/employee/voice01_img_04.jpg" alt=""></p>
              </div>
            </div>
          </li>
          <li>
           <div class="p-timeline-content">
             <div class="wrapper">
               <div class="fll sp-fln">
                <h4>09:30　安全用具の点検</h4>
                <p class="p-txt">3ヶ月に1回の定期点検。自分たちの身を守る用具がきちんと揃っているのか、足りないものはないか、余分はないかな
                  ど、番号と現物の確認を行う。棚はいつも整理整頓されているため、大きな問題もなく終了。新しく研修用として模擬の高圧受変電設備が完成し、その研修用の用具がいくつか用意されているので、今後はこれらも確認を行っていく。</p>
                </div>
                <p class="flr sp-fln"><img src="../images/employee/voice01_img_05.jpg" alt=""></p>
              </div>
            </div>
          </li>
          <li>
           <div class="p-timeline-content">
             <h4>10:00　事務作業開始</h4>
             <p class="p-txt">報告、連絡、相談の時間。資格取得に向けて年次点検が実務経験として認められるので、1件でも多くの年次点検を経験できるよう、先輩のスジュールをチェックしたり、声をかけたりと貪欲に同行を依頼。一方で自分自身の実務経歴書作成も重要な業務。実施した年次点検を振り返り、早く一人前になれるように日々動き続けていく。<br>わからなかったことは、まずは自分で答えを考え、その上で相談する。自分はこう考えますが、これで良いのでしょうか？と。そうすることで、自分で調べたり、考えたりするようになり、幅広い知識を吸収することができる。間違っていれば修正をして再度、自分の頭の中を整理する。</p>
           </div>
         </li>
         <li>
           <div class="p-timeline-content">
             <h4>13:00　昼食後にセミナーの準備</h4>
           </div>
         </li>
         <li>
           <div class="p-timeline-content">
             <div class="wrapper">
               <div class="fll sp-fln">
                <h4>13:30　技術セミナー開始</h4>
                <p class="p-txt">技術者さん向けに開催されるセミナーに参加。勉強になるので、必ず参加するようにしている。今日の内容は、事故事例が2例紹介された。感電事故の事例からどんな注意点が必要だったのか、なぜ起きたのかなどを参加者と共有。安全作業基準マニュアルの見直しをする。服装についても細かく規定があり、自身がモデルになって、みなさんに良い点、悪い点を指摘いただく。さらにDVDにて実際に感電事故に合われた方の映像を見ることで、電気の怖さを改めて知り、本当に安全が一番だと実感する。 </p>
              </div>
              <p class="flr sp-fln"><img src="../images/employee/voice01_img_06.jpg" alt=""></p>
            </div>
          </div>
        </li>
        <li>
         <div class="p-timeline-content">
           <h4>16:00　技術セミナー終了</h4>
           <p class="p-txt">反省会を実施。どうすれば技術者さんが前向きに参加できるのかを議論する。</p>
         </div>
       </li>
       <li>
         <div class="p-timeline-content">
           <h4>16:30　準備作業開始</h4>
           <p class="p-txt">翌日の予定を確認、準備。常に前向きに同行させていただくために、積極的に先輩社員に声をかけていく。</p>
         </div>
       </li>
       <li>
         <div class="p-timeline-content">
           <h4>18:00　退社</h4>
           <p class="p-txt">本日もお疲れさまでした。</p>
         </div>
       </li>
       <li>
         <div class="p-timeline-content">
           <h4>VOICE</h4>
           <p class="p-txt">この日は保安用具点検や技術セミナーがあったため、社内にいましたが、普段はほとんど外出しています。今は先輩について、年次点検、月次点検など、どんどんと外に出て、多くの設備を見て、自分の経験にしていきます。</p>
         </div>
       </li>
     </ul>
   </section>
   <div class="wrapper mt70 pb70">
     <section class="p-voice-cont">
       <h3><img src="../images/employee/voice_title_03.png" alt="仕事の息抜き法"></h3>
       <p><img src="../images/employee/voice01_img_07.jpg" alt=""></p>
       <p>好きな歌を聞きながら、ストレッチをして体をほぐします。</p>
     </section>
     <section class="p-voice-cont sp-mt20">
       <h3><img src="../images/employee/voice_title_04.png" alt="休日の過ごし方"></h3>
       <p><img src="../images/employee/voice01_img_08.jpg" alt=""></p>
       <p>友人や同期と旅行に行ったり、会社のサークル活動で体を動かすなど、<br>皆と交流を深めながら楽しんでいます。</p>
     </section>
   </div>
   <section>
     <h3 class="p-blue_bg pl20">就活生への応援メッセージ</h3>
     <div class="wrapper">
       <p class="p-mg-img"><img src="../images/employee/voice01_img_09.jpg" alt=""></p>
       <p class="p-mg-txt">就活をするに当たってまだ自分がどういう道に進みたいか決まっていないという方もいると思います。そういった方はインターン制度や気になる企業の説明会に、まずは参加してみる事をお勧めします。<br>必ず自分がやりたいと思える道がありますので、妥協せず最後まであきらめずにがんばってください！！</p>
     </div>
   </section>
 </section>
</div>
<section class="p-inquiry mt50">
  <div class="l-wrap-02">
    <div class="p-inquiry-box">
      <div class="p-box-left">
        <div class="sp_none">
          <a href="../seminar/"><img class="imghover" src="../images/employee/seminar_bnr.png" alt="SEMINAR" style="opacity: 1;"></a>
        </div>
        <div class="pc_none">
          <a href="../seminar/">
            <div class="p-text-left">
              <img src="../images/employee/sp_seminar_bnr_01.png" alt="SEMINAR">
            </div>
            <div class="p-text-right">
              <img src="../images/employee/sp_seminar_bnr_02.png" alt="SEMINAR">
            </div>
          </a>
        </div>
      </div>
      <div class="p-box-right">
        <a href="../internship/"><img class="imghover" src="../images/employee/intern_bnr.png" alt="INTERNSHIP" style="opacity: 1;"></a>
      </div>
    </div>
  </div>
</section>
<!-- l-content --></div>


<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



