<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

  <title>坂爪 俊仁 | 社員紹介 | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
  <meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
  <meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報,インタビュー,営業">
  <link rel="canonical" href="#">

  <!-- ページ共通のCSSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
  <!-- ページ共通のCSSファイル終了-->

  <!-- ページ共通のJSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
  <!-- ページ共通のJSファイル終了-->

  <!-- ページ固有のCSSファイル開始-->
  <link rel="stylesheet" href="../css/employee.css">
  <!-- ページ固有のCSSファイル終了-->

  <!-- ページ固有のJSファイル開始-->
  <!-- ページ固有のJSファイル終了-->

  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

  <div class="l-pageBody">

    <nav class="l-topicPath">
      <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="./">
            <span itemprop="name">社員紹介</span></a>
            <meta itemprop="position" content="1" />
          </li>
          <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            ＞<a itemprop="item" href="voice02.html">
            <span itemprop="name">坂爪　俊仁</span></a>
            <meta itemprop="position" content="2" />
          </li>
        </ol>
      </nav>

      <div class="l-content">
        <section class="p-voice02">
          <div class="p-mv">
            <h2><img src="../images/employee/voice02_mv_title.png" alt="SAKATSUME SHUNTO"></h2>
            <p class="p-sub-title mt35 sp-mt20"><img src="../images/employee/voice02_mv_txt.png" alt="イメージ通りの「面白い会社・元気な会社」説明会に参加したことで、この会社と決めた"></p>
            <div class="p-mv-box">
              <p>坂爪　俊仁<br>東日本営業部　首都圏支店　第一係<br>駿河台大学法学部卒　2012年新卒入社</p>
            </div>
          </div>

          <div class="p-voice-wrap">
            <div class="wrapper mt50">
              <section class="p-voice-cont">
                <h3><img src="../images/employee/voice_title_01.png" alt="入社のきっかけ"></h3>
                <p>「本社が高いところにある会社だからというと笑われてしまいますが、それも説明会参加のきっかけでした」。<br>説明会に参加してみて、「面白い会社」だと感じたとともに、電気保安管理業務で民間1位であり、法律を変えた会社であることなどが決め手になりました。<br>警察官や消防官になりたいとの思いがありましたが、<br>説明会に参加したことで、この会社と決めて動きました。</p>
              </section>
              <section class="p-voice-cont sp-mt20">
                <h3><img src="../images/employee/voice_title_02.png" alt="入社してみて感じたこと"></h3>
                <p>イメージ通りの「元気な会社」です。研修が充実しており、基礎をしっかりと積んだうえで、首都圏営業部に配属になりました。<br>これまでに2名の所長のもとで仕事をしていますが、両名ともに社内でも有名で、トップセールスから所長になられた方です。<br>そんな先輩方のもとにいることだけでも自慢ですが、それだけに早く追いつかないといけないと感じています。</p>
              </section>
            </div>
            <section class="pb70">
              <h3 class="p-bd mt40"><img src="../images/employee/voice_title_08.png" alt="新卒社員の一日　ONEDAY"></h3>
              <ul class="p-timeline">
                <li>
                  <div class="p-timeline-content">
                    <h4>08:30　出社</h4>
                    <p class="p-txt">所長に商談の相談とアドバイスをもらう。<br>厳しいが、メリハリがあり、いつも的確なアドバイスで所員を引っ張る姿が印象的。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>08:45　朝礼</h4>
                        <p class="p-txt">声出し、行動力基本動作10カ条唱和、本日の目標発表。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice02_img_01.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>09:00</h4>
                    <p class="p-txt">本日は契約予定が2件あるので、書類など最終チェック。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>09:15　営業所出発</h4>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>10:50　A社へ定期訪問</h4>
                        <p class="p-txt">昨年の展示会で名刺交換をしたお客様へ定期訪問。拠点を多く持っている規模の大きなお客様なので、営業推進部MC課の先輩に同行を依頼。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice02_img_02.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <div class="wrapper">
                      <div class="fll sp-fln">
                        <h4>11:00　製造業B社訪問</h4>
                        <p class="p-txt">新たに倉庫兼事務所として買い上げたビルに訪問。1階と2階の半分が倉庫で、2階の半分と3階、4階が事務所という大きなビルである。エネルギーマネジメントや環境保全における現状や今後の課題、解決策の方向性などをヒアリングする。<br>最後に、創エネルギーや畜エネルギーに対する考えも聞くことができ、今後の提案のヒントをいただく。また、各事業場の統括責任者についてや、導入に至る決裁の流れなど、営業活動に関しての重要な事項もあわせて聞くことができた。</p>
                      </div>
                      <p class="flr sp-fln"><img src="../images/employee/voice02_img_03.jpg" alt=""></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>12:15</h4>
                    <p class="p-txt">B社からの帰り道で先輩と今後の営業プロセスについて打ち合わせ。</p>
                  </div>
                </li>
                <li>
                  <div class="p-timeline-content">
                    <h4>14:00太陽光発電所運営会社<br>C社との契約手続き</h4>
                    <p class="p-txt">前日、先方より「じゃ、お願いするよ」と内諾をもらっていた会社。契約書への署名、捺印が完了したとの一報を受
                      けて再度訪問した。すべての書類に不備がないことを確認し、無事契約に至った。</p>
                    </div>
                  </li>
                  <li>
                    <div class="p-timeline-content">
                      <h4>14:40　八王子への移動中に車中で昼食</h4>
                      <p class="p-txt">最近はまもなく始まる社会人ラグビーチームの練習に向けて、ダイエット中。昼食を抜く事もあるが、八王子の商談は勝負がかかっているので、「腹が減っては戦は出来ぬ」と、少し食べながら移動。</p>
                    </div>
                  </li>
                  <li>
                    <div class="p-timeline-content">
                      <h4>15:00　製造業/パン工場D社訪問</h4>
                      <p class="p-txt">担当エリアに本社があり、導入後の削減実績が出ているため、取締役より八王子工場への提案も依頼された。先週、八王子工場の責任者に提案は済んでおり、本日は契約に向けての訪問である。取締役もわざわざ八王子まで来ていただき同席。<br>提案を後押ししてくれたこともあり、スムーズな契約となった。アフターフォローによる削減実績から得られた信頼の結果である。</p>
                      <div class="wrapper mt40">
                        <p class="flr sp-fln"><img src="../images/employee/voice02_img_04.jpg" alt=""></p>
                        <p class="fll sp-fln txt">契約書類の説明はしっかりと丁寧に抜け漏れがないように。スムーズにする。先方と一緒に考えることは、アフターフォローの第一歩である。</p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="p-timeline-content">
                      <h4>17:10</h4>
                      <p class="p-txt">パン製造なので、八王子工場の責任者は早朝からの勤務にも関わらず、最後まで真剣に設置場所を考えてくれた。</p>
                    </div>
                  </li>
                  <li>
                    <div class="p-timeline-content">
                      <h4>17:20</h4>
                      <p class="p-txt">先方にご挨拶をしてから、キュービクルの位置や周辺設備の確認をして、営業所に戻る。<br>途中休憩時に本日の営業日報を書き上げておく。</p>
                    </div>
                  </li>
                  <li>
                    <div class="p-timeline-content">
                      <div class="wrapper">
                        <div class="fll sp-fln">
                          <h4>18:50　帰社</h4>
                          <p class="p-txt">契約の報告をすると、所長から握手を求められた。</p>
                        </div>
                        <p class="flr sp-fln"><img src="../images/employee/voice02_img_05.jpg" alt=""></p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="p-timeline-content">
                      <h4>19:00　終礼</h4>
                      <p class="p-txt">本日の成果報告。2件の契約があったので、自信を持って報告。所長からその要因を聞かれ、あらためてアフターフォローの重要性を認識する。<br>この日は火曜日だったため、週末までの着地見込みを共有し、それに向けての対策を話し合う。また、来月以降の商談案件の発掘をするよう指示が出た。常に前倒しで進めていく。</p>
                    </div>
                  </li>
                  <li>
                    <div class="p-timeline-content">
                      <h4>20:00　本日の業務終了</h4>
                      <p class="p-txt">お疲れ様でした。</p>
                    </div>
                  </li>
                </ul>
              </section>
              <div class="wrapper pb70">
                <section class="p-voice-cont">
                  <h3><img src="../images/employee/voice_title_03.png" alt="仕事の息抜き法"></h3>
                  <p><img src="../images/employee/voice02_img_06.jpg" alt=""></p>
                  <p>仕事終わりのサークル活動です！アクティブにリフレッシュ！</p>
                </section>
                <section class="p-voice-cont sp-mt20">
                  <h3><img src="../images/employee/voice_title_04.png" alt="休日の過ごし方"></h3>
                  <p><img src="../images/employee/voice02_img_07.jpg" alt=""></p>
                  <p>学生時代からラグビーを続けていて、<br>試合も年に数回行うので日々鍛えています。<br>アウトドア派で、友人達と車・バイク・ＢＢＱと充実した日々を楽しんでいます。</p>
                </section>
              </div>
              <section>
                <h3 class="p-blue_bg">就活生への応援メッセージ</h3>
                <div class="wrapper">
                  <p class="p-mg-img mr40 sp-center sp-mt20"><img src="../images/employee/voice02_img_08.jpg" alt=""></p>
                  <p class="p-mg-txt">就職は学生から社会人になるための大きな一歩です。<br>後悔せずに頑張って下さい！</p>
                </div>
              </section>
            </div>
          </section>

          <section class="p-inquiry mt50">
            <div class="l-wrap-02">
              <div class="p-inquiry-box">
                <div class="p-box-left">
                  <div class="sp_none">
                    <a href="../seminar/"><img class="imghover" src="../images/employee/seminar_bnr.png" alt="SEMINAR" style="opacity: 1;"></a>
                  </div>
                  <div class="pc_none">
                    <a href="../seminar/">
                      <div class="p-text-left">
                        <img src="../images/employee/sp_seminar_bnr_01.png" alt="SEMINAR">
                      </div>
                      <div class="p-text-right">
                        <img src="../images/employee/sp_seminar_bnr_02.png" alt="SEMINAR">
                      </div>
                    </a>
                  </div>
                </div>
                <div class="p-box-right">
                  <a href="../internship/"><img class="imghover" src="../images/employee/intern_bnr.png" alt="INTERNSHIP" style="opacity: 1;"></a>
                </div>
              </div>
            </div>
          </section>
          <!-- l-content --></div>


          <!-- l-pageBody --></div>

          <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
          <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
        </body>
        <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
        </html>



