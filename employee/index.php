<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

  <title>社員紹介| RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
  <meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
  <meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報,インタビュー,営業">
  <link rel="canonical" href="#">

  <!-- ページ共通のCSSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
  <!-- ページ共通のCSSファイル終了-->

  <!-- ページ共通のJSファイル開始-->
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
  <!-- ページ共通のJSファイル終了-->

  <!-- ページ固有のCSSファイル開始-->
  <link rel="stylesheet" href="../css/employee.css">
  <!-- ページ固有のCSSファイル終了-->

  <!-- ページ固有のJSファイル開始-->
  <script type="text/javascript">
    function smartRollover() {
      if(document.getElementsByTagName) {
        var images = document.getElementsByTagName("img");
        for(var i=0; i < images.length; i++) {
          if(images[i].getAttribute("src").match("_off."))
          {
            images[i].onmouseover = function() {
              this.setAttribute("src", this.getAttribute("src").replace("_off.", "_on."));
            }
            images[i].onmouseout = function() {
              this.setAttribute("src", this.getAttribute("src").replace("_on.", "_off."));
            }
          }
        }
      }
    }
    if(window.addEventListener) {
      window.addEventListener("load", smartRollover, false);
    }
    else if(window.attachEvent) {
      window.attachEvent("onload", smartRollover);
    }
  </script>
  <!-- ページ固有のJSファイル終了-->

  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
  <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

  <div class="l-pageBody">

    <nav class="l-topicPath">
      <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="./">
            <span itemprop="name">社員紹介</span></a>
            <meta itemprop="position" content="1" />
          </li>
        </ol>
      </nav>

      <div class="l-content">
        <!-- l-content -->
        <section class="p-employee">
          <h2 class="p-bd tac"><img class="sp-w65p" src="../images/employee/title.jpg" alt="Member 社員紹介"></h2>
          <ul class="p-memberList">
            <li><a href="voice02.php"><img src="../images/employee/img_01_off.jpg" alt=""><p class="pc_none"><span class="p-name">坂爪&emsp;俊仁</span><br />首都圏支店<a href="">&emsp;</a>第一係</p></a></li>
            <li><a href="voice05.php"><img src="../images/employee/img_02_off.jpg" alt=""><p class="pc_none"><span class="p-name">倉元&emsp;梨沙</span><br />営業部&emsp;中部支店</p></a></li>
            <li><a href="voice07.php"><img src="../images/employee/img_03_off.jpg" alt=""><p class="pc_none"><span class="p-name">池田&emsp;武司</span><br />電力システム本部<br>電力事業部&emsp;電力取引課</p></a></li>
            <li><a href="voice09.php"><img src="../images/employee/img_04_off.jpg" alt=""><p class="pc_none"><span class="p-name">福田&emsp;貴志</span><br />立川営業所&emsp;所長</p></a></li>
            <li><a href="voice08.php"><img src="../images/employee/img_05_off.jpg" alt=""><p class="pc_none"><span class="p-name">中塚&emsp;理奈</span><br />営業部&emsp;首都圏支店&emsp;第五係</p></a></li>
            <li><a href="voice03.php"><img src="../images/employee/img_06_off.jpg" alt=""><p class="pc_none"><span class="p-name">森&emsp;啓騎</span><br />技術サービス部&emsp;第一課第三係</p></a></li>
            <li><a href="voice01.php"><img src="../images/employee/img_07_off.jpg" alt=""><p class="pc_none"><span class="p-name">佐藤&emsp;和広</span><br />電力システム本部<br>保安部&emsp;東日本担当</p></a></li>
            <li><a href="voice04.php"><img src="../images/employee/img_08_off.jpg" alt=""><p class="pc_none"><span class="p-name">西川&emsp;愛子</span><br />営業企画部</p></a></li>
            <li><a href="voice06.php"><img src="../images/employee/img_09_off.jpg" alt=""><p class="pc_none"><span class="p-name">大西&emsp;夏子</span><br />保安本部&emsp;管理・教育担当<br>電力事業部&emsp;電力取引課</p></a></li>
          </ul>
        </section>
        <section class="p-inquiry">
          <div class="l-wrap-02">
            <div class="p-inquiry-box">
              <div class="p-box-left">
                <div class="sp_none">
                  <a href="../seminar/"><img class="imghover" src="../images/employee/seminar_bnr.png" alt="SEMINAR" style="opacity: 1;"></a>
                </div>
                <div class="pc_none">
                  <a href="../seminar/">
                    <div class="p-text-left">
                      <img src="../images/employee/sp_seminar_bnr_01.png" alt="SEMINAR">
                    </div>
                    <div class="p-text-right">
                      <img src="../images/employee/sp_seminar_bnr_02.png" alt="SEMINAR">
                    </div>
                  </a>
                </div>
              </div>
              <div class="p-box-right">
                <a href="../internship/"><img class="imghover" src="../images/employee/intern_bnr.png" alt="INTERNSHIP" style="opacity: 1;"></a>
              </div>
            </div>
          </div>
        </section>
      </div>


      <!-- l-pageBody --></div>

      <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
      <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
    </body>
    <?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
    </html>



