<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

<title>日本テクノ株式会社 2018年新卒採用サイト</title>
<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報">
<link rel="canonical" href="<?php echo (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]?>">

<!-- ページ共通のCSSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
<!-- ページ共通のCSSファイル終了-->

<!-- ページ共通のJSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
<!-- ページ共通のJSファイル終了-->

<!-- ページ固有のCSSファイル開始-->
<link rel="stylesheet" type="text/css" href="css/top.css?v=1" media="all">
<!-- ページ固有のCSSファイル終了-->

<!-- ページ固有のJSファイル開始-->
<script type="text/javascript" src="js/top.js?v=1"></script>
<!-- ページ固有のJSファイル終了-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

<div class="l-pageBody">

	<div class="l-content">
		<section class="p-mv">
			<div class="l-wrap">
				<div class="pc_none"><img src="images/top/img_top_sp_01.png" alt="メインビジュアルの背景" /></div>
				<div class="p-mvtext">
					<h2><img src="images/top/img_top_02.png" alt="電力全力" /></h2>
				</div>
			</div>
		</section>
		<section class="p-news">
			<div class="l-wrap">
				<div class="p-news-warp sp_none">
					<div class="p-news-title">
						<h3><img src="images/top/img_top_03.png" alt="news" /></h3>
					</div>
					<div class="p-news-content">
						<ul>
							<li><span class="p-content-title">2016.12.31</span><span class="p-content-texts">「新卒社員紹介」に西日本営業部【遠山 知里】の記事を追加しました。</span></li>
							<li><span class="p-content-title">2016.12.31</span><span class="p-content-texts">「新卒社員紹介」に西日本営業部【遠山 知里】の記事を追加しました。</span></li>
							<li><span class="p-content-title">2016.12.31</span><span class="p-content-texts">「新卒社員紹介」に西日本営業部【遠山 知里】の記事を追加しました。</span></li>
						</ul>
					</div>
				</div>
				<div class="p-news-warp pc_none">
					<p>「新卒社員紹介」に西日本営業部<br />【遠山 知里】の記事を追加しました。</p>
				</div>
			</div>
		</section>
		<section class="p-message">
			<div class="l-wrap">
				<div class="p-message-text">
					<h3 class="p-text-01"><img src="images/top/img_top_05.png" alt="MESSAGE" /></h3>
					<p class="p-text-02"><img src="images/top/img_top_06.png" alt="最初からできる人はいない、だから努力すれば私にも出来る" /></p>
					<p class="p-btn-01"><a href="message/">
						<img class="imghover sp_none" src="images/common/btn_more_01.png" alt="more" />
						<img class="imghover pc_none" src="images/common/btn_more_sp_01.png" alt="more" />
					</a></p>
				</div>
			</div>
		</section>
		<section class="p-inquiry">
			<div class="l-wrap-02">
				<div class="p-inquiry-box">
					<div class="p-box-left">
						<div class="sp_none">
						<a href="seminar/"><img class="imghover" src="images/top/img_top_24.png" alt="SEMINAR" /></a>
						</div>
						<div class="pc_none">
							<a href="internship/">
								<div class="p-text-left">
									<img src="images/top/img_top_sp_06.png" alt="SEMINAR" />
								</div>
								<div class="p-text-right">
									<img src="images/top/img_top_sp_07.png" alt="SEMINAR" />
								</div>
							</a>
						</div>
					</div>
					<div class="p-box-right">
						<a href="#"><img class="imghover" src="images/top/img_top_25.png" alt="INTERNSHIP" /></a>
					</div>
				</div>
			</div>
		</section>
		<section class="p-company">
			<div class="l-wrap">
				<h3><img src="images/top/img_top_07.png" alt="COMPANY" /></h3>
				<p class="sp_none">「最初からできる人はいない、だから努力すればわたしにもできる」<br />
				当社で活躍しているのは、前向きにコツコツと努力できる人材。<br />
				ともに励まし合う同期の仲間や、後輩の成長を心から後押しする先輩とともに<br />
				成長を目指せるフィールドが広がっています。</p>
				<p class="pc_none">「最初からできる人はいない、<br />
				だから努力すればわたしにもできる」<br />
				当社で活躍しているのは、前向きにコツコツと努力できる人材。<br />
				ともに励まし合う同期の仲間や、後輩の成長を心から後押しする先輩とともに成長を目指せるフィールドが広がっています。</p>
			</div>
			<div class="l-wrap-02">
				<div class="p-link-box">
					<div class="p-box-left">
						<a href="profile/"><img class="imghover" src="images/top/img_top_08.png" alt="会社概要" /></a>
					</div>
					<div class="p-box-right">
						<a href="information/"><img class="imghover" src="images/top/img_top_09.png" alt="業務案内" /></a>
					</div>
				</div>
			</div>
			<div class="l-wrap">
				<p class="mb0 sp_tac">夢に描いたことは、努力することで必ず実現する。</p>
				<p class="p-concept">「夢・志・計画・実行・達成」</p>
			</div>
		</section>
		<section class="p-member">
			<div class="p-content-mv">
				<h3><img src="images/top/img_top_12.png" alt="member" /></h3>
				<p class="pc_none p-more-btn">
					<a href="#"><img class="imghover" src="images/common/btn_more_sp_01.png" alt="more" /></a>
				</p>
			</div>
			<div class="l-wrap-02">
				<div class="p-members">
					<div class="clearfix">
						<div class="p-member-box">
						<div class="none p-imgload">
							<img src="images/top/img_top_13.png">
							<img src="images/top/img_top_14.png">
							<img src="images/top/img_top_15.png">
							<img src="images/top/img_top_16.png">
							<img src="images/top/img_top_17.png">
							<img src="images/top/img_top_18.png">
							<img src="images/top/img_top_13_on.png">
							<img src="images/top/img_top_14_on.png">
							<img src="images/top/img_top_15_on.png">
							<img src="images/top/img_top_16_on.png">
							<img src="images/top/img_top_17_on.png">
							<img src="images/top/img_top_18_on.png">
						</div>
							<a class="p-memver-01" href="employee/voice02.php"></a>
							<p class="pc_none">
								<span class="p-name">坂爪　俊仁</span><br />
								首都圏支店　第一係
							</p>
						</div>
						<div class="p-member-box">
							<a class="p-memver-02" href="employee/voice09.php"></a>
							<p class="pc_none">
								<span class="p-name">福田　貴志</span><br />
								立川営業所
							</p>
						</div>
						<div class="p-member-box">
							<a class="p-memver-03" href="employee/voice08.php"></a>
							<p class="pc_none">
								<span class="p-name">中塚　理奈</span><br />
								首都圏支店
							</p>
						</div>
						<div class="p-member-box sp_none">
							<a class="p-memver-04" href="employee/voice03.php"></a>
						</div>
						<div class="p-member-box sp_none">
							<a class="p-memver-05" href="employee/voice05.php"></a>
						</div>
						<div class="p-member-box sp_none">
							<a class="p-memver-06" href="employee/voice01.php"></a>
						</div>
					</div>
				</div>
			</div>
			<div class="l-wrap-02">
				<div class="p-link-box">
					<div class="p-box-left">
						<a href="questionnaire/"><img class="imghover" src="images/top/img_top_19.png" alt="ENQUETE" /></a>
					</div>
					<div class="p-box-right">
						<a href="https://youtu.be/OE8NV0NVkzI" target="_blank" rel="nofollow"><img class="imghover" src="images/top/img_top_20.png" alt="動画" /></a>
					</div>
				</div>
			</div>
		</section>
		<section class="p-recruit">
			<div class="l-wrap-02">
				<h3><img src="images/top/img_top_21.png" alt="RECRUIT" /></h3>
				<div class="clearfix mb70 sp_mb20">
					<div class="p-links">
						<a href="#">
							<img class="imghover sp_none" src="images/top/img_top_26.png" alt="職種紹介" />
							<img class="imghover pc_none" src="images/top/img_top_sp_13.png" alt="職種紹介" />
						</a>
					</div>
					<div class="p-links">
						<a href="#">
							<img class="imghover sp_none" src="images/top/img_top_27.png" alt="募集要項" />
							<img class="imghover pc_none" src="images/top/img_top_sp_14.png" alt="募集要項" />
						</a>
					</div>
					<div class="p-links">
						<a href="#">
							<img class="imghover sp_none" src="images/top/img_top_28.png" alt="研修・制度紹介" />
							<img class="imghover pc_none" src="images/top/img_top_sp_15.png" alt="研修・制度紹介" />
						</a>
					</div>
				</div>
				<div class="p-link-box">
					<div class="p-box-left">
						<a href="media/">
							<img class="imghover sp_none" src="images/top/img_top_22.png" alt="MEDIA" />
							<img class="imghover pc_none" src="images/top/img_top_sp_16.png" alt="MEDIA" />
						</a>
					</div>
					<div class="p-box-right">
						<a href="seminar/">
							<img class="imghover sp_none" src="images/top/img_top_23.png" alt="ENTRY" />
							<img class="imghover pc_none" src="images/top/img_top_sp_17.png" alt="ENTRY" />
						</a>
					</div>
				</div>
			</div>
		</section>
	<!-- l-content --></div>

<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



