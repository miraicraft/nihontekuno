<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_begin.php'); ?>
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_meta.php'); ?>

<title>メディア | RECRUIT | 日本テクノ株式会社 2018年新卒採用サイト</title>
<meta name="description" content="日本テクノ株式会社 2018年新卒採用特設サイトです。">
<meta name="keywords" content="日本テクノ,新卒,採用情報,会社情報">
<link rel="canonical" href="#">

<!-- ページ共通のCSSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_css.php'); ?>
<!-- ページ共通のCSSファイル終了-->

<!-- ページ共通のJSファイル開始-->
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/common_js.php'); ?>
<!-- ページ共通のJSファイル終了-->

<!-- ページ固有のCSSファイル開始-->
<link rel="stylesheet" type="text/css" href="../css/media.css?v=1" media="all">
<!-- ページ固有のCSSファイル終了-->

<!-- ページ固有のJSファイル開始-->
<!-- ページ固有のJSファイル終了-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/head_end.php'); ?>
</head>

<body id="pagetop">
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_begin.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/header.php'); ?>

<div class="l-pageBody">

	<div class="l-content">
        <div class="p-media-inner">
        <h2>MEDIA</h2>
             <div class="p-each-media">
             <h3 class="p-upper-title">コーポレート<br />サイト</h3>
            <a href="http://www.n-techno.co.jp/"><img class="imghover" src="../images/media/media_out_01.png" alt="コーポレートサイト"/></a>
             </div>
             <div class="p-each-media">
             <h3 class="p-upper-title">SMART CLOCK</h3>
             <a href="http://n-techno.co.jp/smartclock/"><img class="imghover" src="../images/media/media_out_02.png" alt="SMART CLOCK"/></a>
             </div>
             <div class="p-each-media">
             <h3 class="p-upper-title">ERIA</h3>
             <a href="http://www.eria.jp/"><img class="imghover" src="../images/media/media_out_03.png" alt="ERIA"/></a>
             </div>
             <div class="p-each-media">
             <h3 class="p-upper-title">Eco News<br />Web magazine</h3>
             <a href="http://econews.jp/"><img class="imghover" src="../images/media/media_out_04.png" alt="Eco News Web magazine"/></a>
             </div>
             <div class="p-each-media">
             <h3 class="p-under-title pc_none">YOUTUBE</h3>
             <a href="https://www.youtube.com/watch?v=oh5OOj1dh08"><img class="imghover" src="../images/media/media_out_05.png" alt="YOUTUBE"/></a>
             <h3 class="p-under-title sp_none">YOUTUBE</h3>
             </div>
             <div class="p-each-media">
             <h3 class="p-under-title pc_none">facebook</h3>
             <a href="https://www.facebook.com/nihontechno.saiyo"><img class="imghover" src="../images/media/media_out_06.png" alt="Facebook"/></a>
             <h3 class="p-under-title sp_none">facebook</h3>
             </div>
             <div class="p-each-media">
             <h3 class="p-under-title pc_none">省エネの達人サイト</h3>
             <a href="http://www.eco-tatsujin.jp/"><img class="imghover" src="../images/media/media_out_07.png" alt="省エネの達人サイト" /></a>
             <h3 class="p-under-title sp_none">省エネの達人サイト</h3>
             </div>
              <div class="p-each-media">
              <h3 class="p-under-title pc_none">menbers site</h3>
             <a href="http://www.n-techno.org/"><img class="imghover" src="../images/media/media_out_08.png"　alt="menbers site" /></a>
             <h3 class="p-under-title sp_none">menbers site</h3>
             </div>

        <!--p-media-inner--></div>
	<!-- l-content --></div>


<!-- l-pageBody --></div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/body_end.php'); ?>
</body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/common_units/analysis_tags_areas/html_end.php'); ?>
</html>



